"use strict";

var _http = require("http");

var _http2 = _interopRequireDefault(_http);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
app.set('view engine', 'pug');
app.set('views', 'wwwroot/views');

app.get('/', function (req, res) {
	res.render('index', { title: 'Hey', message: 'family tree' });
});

var server = _http2.default.createServer(app);

server.listen(3000);
console.log('-----------------------server is running ------------------------------');