import http from "http";
import express from "express";

let app = express();

app.set('view engine', 'pug');
app.set('views', 'wwwroot/views');

app.get('/',(req, res) => {
	  res.render('index', { title: 'Family tree', message: 'family tree'});
});

var server = http.createServer(app);

server.listen(3000);
console.log('-----------------------server is running ------------------------------');
