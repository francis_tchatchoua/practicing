﻿using GameEngineUI.Players;

namespace GameEngineUI
{
    public class GameResult
    {
        public string Winner { get; set; }
        public bool HasWinner { get; set; }
    }
}
