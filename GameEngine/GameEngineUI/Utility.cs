﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngineUI
{
    public class Utility
    {
        public static IDictionary<string, string> WinCobinations = new Dictionary<string, string>()
        {
            ["rock|scissors"] = "rock",
            ["scissors|rock"] = "rock",
            ["scissors|paper"] = "scissors",
            ["paper|scissors"] = "scissors",
            ["paper|rock"] = "paper",
            ["rock|paper"] = "paper"
        };
    }
}
