﻿using System;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using GameEngineUI.Services;

namespace GameEngineUI
{
    class Program
    {
        static void Main(string[] args)
        {
            var moves = ConfigurationManager.AppSettings["GameMove"].Split(',').Select(m=>m).ToList();

            var playerService= new PlayerService();
            var gameService = new GameService(playerService);

            var players = playerService.GetPlayers().ToList();

            Console.WriteLine($"**** Welcome to {string.Join(",",moves)} ****");
            Console.WriteLine("------- Please use the code below to select a player type ------");

            foreach (var player in players)
            {
                Console.WriteLine(player);
            }
            
           var player1Key = gameService.GetSelectedPlayerKey();
           var player2Key = gameService.GetSelectedPlayerKey();

            var player1 = PlayerFactory.GetPlayer(player1Key);
            var player2 = PlayerFactory.GetPlayer(player2Key);

            var result = gameService.Play(player1, player2);

            Console.WriteLine(!result.HasWinner
                ? "Couldn't get a winner for this game"
                : $"!!!!!!!!!!!!!! THE WINNER IS <{result.Winner}> !!!!!!!!!!!!!!!!");


            Console.Read();
        }
    }
}
