﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEngineUI.Players;

namespace GameEngineUI
{
    public class PlayerFactory
    {
        public static Player GetPlayer(string playerKey)
        {
            switch (playerKey)
            {
                case "HP":
                    return new HumanPlayer();
                case "RP":
                    return new RandomPlayer();
                case "TP":
                    return new TacticalPlayer();

                default:
                    return new NullPlayer();

            }
        }
    }
}
