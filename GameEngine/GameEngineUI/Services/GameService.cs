﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using GameEngineUI.Players;

namespace GameEngineUI.Services
{
    public class GameService:IGameService
    {
        int _playerCount = 0;
        readonly IPlayerService _playerService;

        public GameService(IPlayerService playerService)
        {
            _playerService = playerService;
        }
        public string GetSelectedPlayerKey()
        {
            var validSelection = false;

            var attempt = 0;

            var key = string.Empty;

            while (!validSelection && attempt <5)
            {
                Console.WriteLine($"Player {_playerCount+1}  please select your player type");
                key = Console.ReadLine();

                validSelection = _playerService.IsPlayerTypeValid(key);
                attempt = attempt + 1;
            }

            if (attempt == 5)
            {
                Console.WriteLine("the game is terminated invalid player selected");
                throw new InvalidOperationException();
            }

            _playerCount =+ 1;
            return key;
        }

        public GameResult Play(Player player1, Player player2)
        {
            var gameResult=new GameResult();

            var numberOfRound = int.Parse(ConfigurationManager.AppSettings["MaxNumberOfGame"]);

            var player1WinCount = 0;
            var player2WinCount = 0;
            var round = 1;

            while (numberOfRound >0)
            {
                Console.WriteLine($" ********* Round {round} ******************");

                Console.WriteLine("Player 1:");
                var playe1Move = player1.Play().ToLower();

                Console.WriteLine("Player 2:");
                var playe2Move = player2.Play().ToLower();

                if (Utility.WinCobinations.ContainsKey($"{playe1Move}|{playe2Move}"))
                {
                    var winningMove = Utility.WinCobinations[$"{playe1Move}|{playe2Move}"];

                    if (winningMove == playe1Move)
                    {
                        player1WinCount += 1;
                    }
                    else
                    {
                        player2WinCount += 1;
                    }
                    
                }

                round += 1;
                numberOfRound -= 1;
            }

            gameResult.HasWinner = player2WinCount != player1WinCount;

            if (gameResult.HasWinner)
            {
                gameResult.Winner = player1WinCount > player2WinCount ? "Player 1" : "player 2";
            }

            return gameResult;
        }
    }
}
