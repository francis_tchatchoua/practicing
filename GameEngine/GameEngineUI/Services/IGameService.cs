﻿using System.Collections.Generic;
using GameEngineUI.Players;

namespace GameEngineUI.Services
{
    public interface IGameService
    {
        string GetSelectedPlayerKey();
        GameResult Play(Player player1, Player player2);
    }
}
