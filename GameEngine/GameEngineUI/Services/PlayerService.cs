﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameEngineUI.Players;

namespace GameEngineUI.Services
{
    public class PlayerService: IPlayerService
    {
        public IEnumerable<Player> GetPlayers()
        {
            return typeof(Player).Assembly.GetTypes()
                .Where(type => typeof(Player).IsAssignableFrom(type) && typeof(Player) != type)
                .Select(Activator.CreateInstance).Cast<Player>();

        }
        public bool IsPlayerTypeValid(string playerType)
        {
            if (string.IsNullOrWhiteSpace(playerType)) return false;

            return GetPlayers().Select(p => p.Key).Any(key => key == playerType.Trim());
        }
    }
}
