﻿using System.Collections.Generic;
using GameEngineUI.Players;

namespace GameEngineUI.Services
{
    public interface IPlayerService
    {
        IEnumerable<Player> GetPlayers();
        bool IsPlayerTypeValid(string playerType);
    }
}
