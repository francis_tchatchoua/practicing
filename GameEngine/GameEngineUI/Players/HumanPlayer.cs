﻿using System;
using System.CodeDom;
using System.Configuration;
using System.Linq;

namespace GameEngineUI.Players
{
    public class HumanPlayer:Player
    {
        const string KEY ="HP";
        public override string Key => KEY;

        public override string Play()
        {
            var validMove = false;
            var moves = ConfigurationManager.AppSettings["GameMove"].Split(',').Select(m => m.ToLower().Trim()).ToList();
            var attempt = 0;

            var move = string.Empty;

            while (!validMove && attempt < 5)
            {
                Console.WriteLine("Select your move .....");
                move = Console.ReadLine();

                validMove = moves.Any(x => x.ToLower().Trim() == move);
                attempt = attempt + 1;
            }

            if (attempt == 5)
            {
                Console.WriteLine("the game is terminated invalid move selected");
                throw new InvalidOperationException();
            }
            return move;
        }

        public override string ToString()
        {
            return $"Press {Key} for Human Player";
        }
    }
}
