﻿using System;
using System.Configuration;
using System.Linq;

namespace GameEngineUI.Players
{
    public class TacticalPlayer:Player
    {
        const string KEY = "TP";
        public override string Key => KEY;
        string _lastMove = string.Empty;

        public override string Play()
        {
            var move = string.Empty;

            if (string.IsNullOrWhiteSpace(_lastMove))
            {
                _lastMove = GetRandomMove();

                Console.WriteLine(_lastMove);

                return _lastMove;
            }

            var moves = ConfigurationManager.AppSettings["GameMove"].Split(',').Select(m => m.ToLower().Trim()).ToList();

            var combinations = Utility.WinCobinations.Where(x => x.Value.ToLower().Trim() != _lastMove);

            move = combinations.Where(x => x.Key.Split('|').Any(k => k == _lastMove))
                .SelectMany(x => x.Key.Split('|'))
                .FirstOrDefault(x => x != _lastMove);

            Console.WriteLine(move);

            return move;
        }
        public override string ToString()
        {
            return $"Press {Key} for Tactical Player";
        }
    }
}
