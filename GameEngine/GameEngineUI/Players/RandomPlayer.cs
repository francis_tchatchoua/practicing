﻿using System;
using System.Configuration;
using System.Linq;

namespace GameEngineUI.Players
{
    public class RandomPlayer:Player
    {
        const string KEY = "RP";
        public override string Key => KEY;

        public override string ToString()
        {
            return $"Press {Key} for Random Player";
        }

        public override string Play()
        {
            var move =GetRandomMove();
            Console.WriteLine(move);

            return move;
        }
    }
}
