﻿using System;
using System.Configuration;
using System.Linq;

namespace GameEngineUI.Players
{
    public abstract class Player
    {
        public abstract string Play();
        public abstract string Key { get;}

        protected string GetRandomMove()
        {
            var moves = ConfigurationManager.AppSettings["GameMove"].Split(',').Select(m => m.ToLower().Trim()).ToList();

            var rdnGen = new Random();

            var rand = rdnGen.Next(0, moves.Count - 1);

            return moves.ElementAt(rand);
        }
    }
}
