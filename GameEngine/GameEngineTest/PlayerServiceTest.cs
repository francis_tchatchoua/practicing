﻿using System.Linq;
using GameEngineUI.Services;
using NUnit.Framework;

namespace GameEngineTest
{
    [TestFixture]
    public class PlayerServiceTest
    {
        private readonly IPlayerService _service =new PlayerService();
        [Test]
        public void GetPlayers_should_return_four_players()
        {
            var players = _service.GetPlayers().ToList();

            Assert.AreEqual(4,players.Count);
        }
        [Test]
        public void IsPlayerTypeValid_should_return_false()
        {
            var valid = _service.IsPlayerTypeValid("");

            Assert.IsFalse(valid);
        }
        [Test]
        public void IsPlayerTypeValid_should_return_true()
        {
            var valid = _service.IsPlayerTypeValid("HP ");

            Assert.IsTrue(valid);
        }
    }
}
