﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FrisbyRestApiDemo.Models;

namespace FrisbyRestApiDemo.Controllers
{
    [RoutePrefix("Clients")]
    public class ClientsController : ApiController
    {
        private static List<Client> _clients;

        public ClientsController()
        {
            _clients = new List<Client>()
            {
                new Client() {Id = 0, Name = "Apple", Description = "Apple Inc"},
                new Client() {Id = 1, Name = "Willis Towers   Wastson", Description = "Willis Towers   Wastson"},
                new Client() {Id = 2, Name = "Moderate Clam", Description = "Moderate Clam"}
            };
        }
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(_clients);
        }
        [Route("{id:int}")]
        public IHttpActionResult Get(int id)
        {
            var client = _clients.Where(c => c.Id == id);
            return Ok(client);
        }

        public IHttpActionResult Post(Client client)
        {
            if(client==null) return BadRequest();

            _clients.Add(client);

            return Created($"api/clients/{client.Id}",client);
        }
        [Route("{id}")]
        public IHttpActionResult Put(int id,[FromBody]Client client)
        {
            if (client == null) return BadRequest();

            var existingClient = _clients.FirstOrDefault(c => c.Id == id);

            if(existingClient==null) return NotFound();

            _clients.Remove(existingClient);

            existingClient.Name = client.Name;
            existingClient.Description = client.Description;

            _clients.Add(existingClient);

            return Ok(existingClient);
        }
    }
}