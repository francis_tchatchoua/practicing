params([String]$RabbitDllPath = "not specified")

Write-Host "Rabbit DLL Path"
Write-Host $RabbitDllPath -ForegroundColor Green

$absoluteRabbitDllPath = Resolve-Path $RabbitDllPath

Write-Host "Absolute Rabbit DLL Path"
Write-Host $absoluteRabbitDllPath -ForegroundColor Green

[Reflection.Assembly]::Load($absoluteRabbitDllPath)

Write-Host "Settingb Up RabbitMQ Connection Factory"
 
$factory = New-Object RabbitMQ.Client.ConnectionFactory
$hostNameProp = [RabbitMQ.Client.ConnectionFactory]::GetField("HostName")
$hostNameProp.SetValue($factory,"localhost")

$userNameProp = [RabbitMQ.Client.ConnectionFactory]::GetField("UserName")
$userNameProp.SetValue($factory,"guest")

$passwordProp = [RabbitMQ.Client.ConnectionFactory]::GetField("Password")
$passwordProp.SetValue($factory,"guest")

$createConnectionMethod= [RabbitMQ.Client.ConnectionFactory]::GetMethod("CreateConnection",[Type]::EmptyTypes)
$connection =$createConnectionMethod.Invoke($factory,"instance,public",$null.$null,$null)


Write-Host "Setting Up RabbitMQ Model"
$model = $connection.CreateModel();

Write-Host "Create Queue"
$model.QueueDeclare("Hello",$false,$false,$false,$null)


Write-Host "Setup Completed"
