﻿using System.Threading.Tasks;
using System.Web.Http;
using FitUk.DAL.Repository.Interfaces;

namespace FitUk.API.Controllers
{
    [RoutePrefix("api/RefreshTokens")]
    public class RefreshTokensController : ApiController
    {
        private readonly IMemberShipRepository _memberShipRepository;

        public RefreshTokensController(IMemberShipRepository memberShipRepository)
        {
            _memberShipRepository = memberShipRepository;
        }

        [Authorize(Users = "Admin")]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(_memberShipRepository.GetAllRefreshTokens());
        }

        //[Authorize(Users = "Admin")]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Delete(string tokenId)
        {
            var result = await _memberShipRepository.RemoveRefreshToken(tokenId);
            if (result)
            {
                return Ok();
            }
            return BadRequest("Token Id does not exist");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _memberShipRepository.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}