﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FitUk.Api.Controllers
{
    [RoutePrefix("api/values")]
    public class ValuesController:ApiController
    {
        [Route("")]
        [Authorize]
        public IHttpActionResult Get()
        {
            return Ok(new List<string>() {"francis", "bob", "rim"});
        }
    }
}