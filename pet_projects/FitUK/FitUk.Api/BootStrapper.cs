﻿using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using FitUk.API;
using FitUk.DAL.Repository;
using FitUk.DAL.Repository.Interfaces;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Serialization;

namespace FitUk.Api
{
    public class BootStrapper
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();
            RegisterRoutes(config);
            RegisterServices(container, config);
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

        private static void RegisterServices(IUnityContainer container, HttpConfiguration config)
        {
            container.RegisterType<IMemberShipRepository, MemberShipRepository>();
            config.DependencyResolver = new DepedencyResolver(container);
        }

        private static void RegisterRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                     "DefaultApi",
                     "api/{controller}/{id}",
                     new { id = RouteParameter.Optional }
                 );
        }
    }
}