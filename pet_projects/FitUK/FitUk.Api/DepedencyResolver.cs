﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Http.Dependencies;
using Microsoft.Practices.Unity;

namespace FitUk.API
{
    public class DepedencyResolver:IDependencyResolver
    {
        protected IUnityContainer Container;

        public DepedencyResolver(IUnityContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException(nameof(Container));
            }
            Container = container;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return Container.Resolve(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return Container.ResolveAll(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return new List<object>();
            }
        }

        public IDependencyScope BeginScope()
        {
            var child = Container.CreateChildContainer();
            return new DepedencyResolver(child);
        }

        public void Dispose()
        {
            Container.Dispose();
        }
    }
}