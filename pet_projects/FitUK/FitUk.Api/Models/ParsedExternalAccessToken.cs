﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace FitUk.API.Models
{
    public class ParsedExternalAccessToken
    {
        [JsonProperty("user_id")]
        public string UserId { get; set; }
        [JsonProperty("app_id")]
        public string AppId { get; set; }
    }
}