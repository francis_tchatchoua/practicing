﻿using System.Data.Entity;
using FitUk.DAL.Enteties;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FitUk.DAL.Database
{
    public class FitUkMembershipContext:IdentityDbContext<IdentityUser>
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public FitUkMembershipContext():base("FitUkMembership")
        {}
    }
}
