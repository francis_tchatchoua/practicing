﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using FitUk.DAL.Repository.Interfaces;

namespace FitUk.DAL.Repository
{
    public class BaseRepository<T, TContext> : IRepository<T>,IDisposable
        where T : class where TContext:DbContext,  new()
    {
        private  TContext _context;
        public BaseRepository()
        {}
        public BaseRepository(TContext context)
        {
            _context = context;
        }

        public TContext Context
        {
            get
            {
                _context = _context ?? new TContext();
                _context.Configuration.LazyLoadingEnabled = true;

                return _context;
            }
        }


        public async Task<int> AddAsync(T entity)
        {
            Context.Set<T>().Add(entity);

            return await Context.SaveChangesAsync();
        }
        public async  Task<T> FindOneAsync(Expression<Func<T, bool>> predicate)
        {
           return await  Context.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public async Task<List<T>> FindAsync(Expression<Func<T, bool>> predicate)
        {
            return await Context.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<List<T>> FindAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public async Task<int> UpdateAsync(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            return await Context.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(T entity)
        {
            Context.Set<T>().Remove(entity);
            return await Context.SaveChangesAsync();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Context?.Dispose();
            }
        }

        protected async Task<int> SaveChangesAsync()
        {
           return await Context.SaveChangesAsync();
        }
    }
}
