﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitUk.DAL.Database;
using FitUk.DAL.Enteties;
using FitUk.DAL.Repository.Interfaces;
using FitUk.Dto.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FitUk.DAL.Repository
{  
    public class MemberShipRepository:BaseRepository<RefreshToken, FitUkMembershipContext>, 
        IMemberShipRepository,IDisposable
    {
        private readonly UserManager<IdentityUser> _userManager;

        public MemberShipRepository()
        {
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(Context));
        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel)
        {
            var user = new IdentityUser
            {
                UserName = userModel.UserName
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            var user = await _userManager.FindAsync(userName, password);

            return user;
        }
        public Client FindClient(string clientId)
        {
            var client = Context.Clients.Find(clientId);

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {

            var existingToken = await FindOneAsync(r => r.Subject == token.Subject && r.ClientId == token.ClientId);

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }
            return await AddAsync(token) >0;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = await Context.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                Context.RefreshTokens.Remove(refreshToken);
                return await Context.SaveChangesAsync() > 0;
            }

            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            Context.RefreshTokens.Remove(refreshToken);
            return await Context.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await Context.RefreshTokens.FindAsync(refreshTokenId);

            return refreshToken;
        }

        public async Task<List<RefreshToken>> GetAllRefreshTokens()
        {
            return await FindAllAsync();
        }
        public async Task<IdentityUser> FindAsync(UserLoginInfo loginInfo)
        {
            var user = await _userManager.FindAsync(loginInfo);

            return user;
        }
        #region This Area is used to enable third party authentication
        public async Task<IdentityResult> CreateAsync(IdentityUser user)
        {
            var result = await _userManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }
        #endregion
        public new void Dispose()
        {
            Context.Dispose();
            _userManager.Dispose();

        }
    }
}
