﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FitUk.DAL.Repository.Interfaces
{
    public  interface IRepository<T>
    {
        Task<int> AddAsync(T entity);
        Task<T> FindOneAsync(Expression<Func<T, bool>> predicate);
        Task<List<T>> FindAsync(Expression<Func<T, bool>> predicate);
        Task<List<T>> FindAllAsync();
        Task<int> UpdateAsync(T entity);
        Task<int> DeleteAsync(T entity);
    }
}
