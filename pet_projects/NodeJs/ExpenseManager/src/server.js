import http from "http";
import express from "express";
import passport from "passport";
import {Strategy} from 'passport-local';
import bodyParser from 'body-parser';
import {BasicStrategy} from 'passport-http';

let app = express();
let viewsDir = __dirname+'/wwwroot/views';

app.use(bodyParser.urlencoded({ extended: false }))
app.use(passport.initialize());
app.use(passport.session());
app.set('view engine', 'pug');
app.set('views', viewsDir);

let anonymousRoute = express.Router();
let adminRoute = express.Router();

passport.use(new Strategy((username, password, done) => {
  	var user ={username:username,password:password};
      return done(null, user);
  	}
));
passport.use(new BasicStrategy((userid, password, done) => {
    var user ={userid:userid,password:password};
      return done(null, user);
    }
));

anonymousRoute.get('/',(req,res) => {
	res.render('index', { title: 'index', message: 'Hello welcome to node!'});
});

adminRoute.post('/');

app.use('/',anonymousRoute);
app.use('/admin',passport.authenticate('basic', { session: false }),(req,res) => {
	res.json({ username: req.user.username, username: req.user.password });
},adminRoute);


var server = http.createServer(app);

server.listen(3000);
console.log('-------------------app listening on port 3000 ------------------------');
