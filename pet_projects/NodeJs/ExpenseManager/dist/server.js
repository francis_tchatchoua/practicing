"use strict";

var _http = require("http");

var _http2 = _interopRequireDefault(_http);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _passport = require("passport");

var _passport2 = _interopRequireDefault(_passport);

var _passportLocal = require("passport-local");

var _bodyParser = require("body-parser");

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _passportHttp = require("passport-http");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var app = (0, _express2.default)();
var viewDir = __dirname + '/wwwroot/views';

app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use(_passport2.default.initialize());
app.use(_passport2.default.session());
app.set('view engine', 'pug');
app.set('views', viewDir);

var anonymousRoute = _express2.default.Router();
var adminRoute = _express2.default.Router();

_passport2.default.use(new _passportLocal.Strategy(function (username, password, done) {
  var user = { username: username, password: password };
  return done(null, user);
}));
_passport2.default.use(new _passportHttp.BasicStrategy(function (userid, password, done) {
  var user = { userid: userid, password: password };
  return done(null, user);
}));

anonymousRoute.get('/', function (req, res) {
  res.render('index', { title: 'index', message: 'Hello welcome to node!' });
});

adminRoute.post('/');

app.use('/', anonymousRoute);
app.use('/admin', _passport2.default.authenticate('basic', { session: false }), function (req, res) {
  res.json(_defineProperty({ username: req.user.username }, "username", req.user.password));
}, adminRoute);

var server = _http2.default.createServer(app);

server.listen(3000);
console.log('-------------------app listening on port 3000 ------------------------');