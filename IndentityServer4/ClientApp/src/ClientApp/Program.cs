﻿using System;
using System.Net.Http;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace ClientApp
{
    public class Program
    {
        public static  void Main(string[] args)
        {
            var disco =  DiscoveryClient.GetAsync("http://localhost:5000");

            var tokenClient = new TokenClient(disco.Result.TokenEndpoint, "client", "secret");
            var tokenResponse =  tokenClient.RequestClientCredentialsAsync("api1");

            if (tokenResponse.Result.IsError)
            {
                Console.WriteLine(tokenResponse.Result.Error);
                return;
            }

            Console.WriteLine(tokenResponse.Result.Json);
            CallApi(tokenResponse.Result);
            Console.Read();
        }

        public static void CallApi(TokenResponse tokenResponse)
        {
            var client = new HttpClient();
            client.SetBearerToken(tokenResponse.AccessToken);

            var response =  client.GetAsync("http://localhost:5001/identity").Result;
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }

            var content = response.Content.ReadAsStringAsync().Result;
            Console.WriteLine(JArray.Parse(content));
        }
    }
}
