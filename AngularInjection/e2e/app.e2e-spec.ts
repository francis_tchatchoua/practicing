import { AngularInjectionPage } from './app.po';

describe('angular-injection App', function() {
  let page: AngularInjectionPage;

  beforeEach(() => {
    page = new AngularInjectionPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
