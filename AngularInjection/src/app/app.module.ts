import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {HighLightRedDirective} from './directives/highlight-red.directive';
import {HighLightYellowDirective} from './directives/highlight-yellow.directive';
import { AppComponent } from './app.component';
import { MeComponent } from './me.component';

@NgModule({
  declarations: [
    AppComponent,
    HighLightRedDirective,
    HighLightYellowDirective,
    MeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
