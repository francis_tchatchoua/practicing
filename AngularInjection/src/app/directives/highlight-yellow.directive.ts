import {Directive,ElementRef,OnInit,Renderer,Host,Injector} from "@angular/core";
import {MyService} from '../services/my.service';

class Test {

}
@Directive({
    selector:"[highlight-yellow]"

})
export class HighLightYellowDirective implements OnInit{
    constructor(@Host() private  service:MyService,
    private element:ElementRef,
    private renderer:Renderer,
    private injector:Injector){
        console.log('HighLightYellowDirective=>',injector.get(MyService));
   
    }

    ngOnInit(){
        debugger;
        this.renderer.setElementStyle(this.element.nativeElement,"backgroundColor","yellow");
    }
}