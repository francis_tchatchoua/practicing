import {Directive,ElementRef,OnInit,Renderer,Injector,Self} from "@angular/core";
import {MyService} from '../services/my.service';
@Directive({
    selector:"[highlight-red]"

})
export class HighLightRedDirective implements OnInit{
    constructor(@Self() private service:MyService,
        private element:ElementRef,
        private renderer:Renderer,
        private injector : Injector){

        console.log('HighLightRedDirective=>',injector.get(MyService));

    }

    ngOnInit(){
      
        this.renderer.setElementStyle(this.element.nativeElement,"backgroundColor","red");
    }
}