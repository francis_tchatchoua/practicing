﻿using System;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;
using TaylorFit.Domain;

namespace web
{
    public class Ioc
    {
        public static IServiceProvider Configure(IServiceCollection services)
        {
            var container = new Container(c => c.AddRegistry<ServiceRegistry>());
            container.Configure(config =>
            {
                config.Scan(_ =>
                {
                    _.AssemblyContainingType(typeof(Startup));
                    _.WithDefaultConventions();
                });

                config.Populate(services);
            });

            return container.GetInstance<IServiceProvider>();
        }
    }
}
