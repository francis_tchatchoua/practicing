﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;
using Interoute.Global.Extensions.Helpers;
using Interoute.Global.Web.Utilities.FilterAttributes;
using Interoute.Hub.Domain.Services.Security;
using Interoute.Hub.Infrastructure.Filters.Api;
using Interoute.Hub.Utility.Permission;
using NLog;

namespace Interoute.MyDocuments.API
{
    [ApiCacheFilter(0)]
    public class CustomerDocumentProxyController : ApiController
    {
        private readonly IOAuth2TokensService _tokensService;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private string CreatePath(object argument)
        {
            return CreatePath(null, argument);
        }

        private string CreatePath(string basePath, object argument)
        {
            var pathStringBuilder = new StringBuilder();
            if (!string.IsNullOrEmpty(basePath))
            {
                pathStringBuilder.Append(basePath);
                if (argument != null)
                {
                    pathStringBuilder.Append("/");
                }
            }
            if (argument != null)
            {
                pathStringBuilder.AppendFormat(argument.ToString());
            }

            return pathStringBuilder.ToString();
        }

        public CustomerDocumentProxyController(IOAuth2TokensService tokensService)
        {
            _tokensService = tokensService;
        }
        
        [HttpGet]
        [SecurityDecoratorFilter("Documents/Download", Permission.View)]
        [ActionName("List")]
        public async Task<HttpResponseMessage> List(int? folderId)
        {
            return await PrepareAndForwardRequest(CreatePath(folderId));
        }

        [HttpPost]
        [SecurityDecoratorFilter("Documents/Download", Permission.View)]
        [DisableCacheHeadersFilter]
        [ActionName("Download")]
        public async Task<HttpResponseMessage> Download(int fileId)
        {
            return await PrepareAndForwardRequest(CreatePath("download", fileId));
        }

        [HttpDelete]
        [SecurityDecoratorFilter("Documents/Delete files", Permission.View)]
        [ActionName("DeleteFile")]
        public async Task<HttpResponseMessage> DeleteFile(int fileId)
        {
            return await PrepareAndForwardRequest(CreatePath(fileId));
        }

        [HttpDelete]
        [SecurityDecoratorFilter("Documents/Delete folders", Permission.View)]
        [ActionName("DeleteFolders")]
        public async Task<HttpResponseMessage> DeleteFolders(int folderId)
        {
            return await PrepareAndForwardRequest(CreatePath(folderId));
        }

        [HttpPost]
        [SecurityDecoratorFilter("Documents/Upload files", Permission.View)]
        [ActionName("UploadFiles")]
        public async Task<HttpResponseMessage> UploadFiles(int? folderId)
        {
            return await PrepareAndForwardRequest(CreatePath("upload", folderId));
        }

        [HttpPost]
        [SecurityDecoratorFilter("Documents/Create folders", Permission.View)]
        [ActionName("CreateFolder")]
        public async Task<HttpResponseMessage> CreateFolder(int? folderId)
        {
            return await PrepareAndForwardRequest(CreatePath("create-dir", folderId));
        }

        [HttpPost]
        [SecurityDecoratorFilter("Documents/Rename files", Permission.View)]
        [ActionName("RenameFile")]
        public async Task<HttpResponseMessage> RenameFile(int fileId)
        {
            return await PrepareAndForwardRequest(CreatePath("rename", fileId));
        }

        [HttpPost]
        [SecurityDecoratorFilter("Documents/Rename folders", Permission.View)]
        [ActionName("RenameFolder")]
        public async Task<HttpResponseMessage> RenameFolder(int folderId)
        {
            return await PrepareAndForwardRequest(CreatePath("rename", folderId));
        }

        private async Task<HttpResponseMessage> PrepareAndForwardRequest(string path)
        {
            var token = _tokensService.GetPricingToolToken();
            var baseUrl = ConfigurationManagerHelper.CustomerDocummentsApi.Replace("https:", "http:");
            Request.RequestUri = new Uri(baseUrl + "files/" + path);
            Request.Headers.Add("Authorization", string.Format("Bearer {0}", token.AccessToken));
            if (Request.Method == HttpMethod.Get)
            {
                Request.Content = null;
            }

            var client = new HttpClient();
            var response = await client.SendAsync(Request, HttpCompletionOption.ResponseHeadersRead);

            return response;
        }
    }
}
