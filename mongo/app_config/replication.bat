cd c:\mongo_server\

md c:\mongo_server\db1
md c:\mongo_server\db2
md c:\mongo_server\db3

@REM Primary
start "Primary" mongod --dbpath ./db1 --port 30000 --replSet "demo"

@REM Secondary
start "Secondary" mongod --dbpath ./db2 --port 40000 --replSet "demo"

@REM Abiter
start "Abiter" mongod --dbpath ./db3 --port 50000 --replSet "demo"
