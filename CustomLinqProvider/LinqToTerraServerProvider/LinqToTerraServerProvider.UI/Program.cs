﻿using System;
using System.Collections.Generic;

namespace LinqToTerraServerProvider.UI
{
    public interface IPerson { }
    public class Person:IPerson { }
    public class Program
    {
        static void Main(string[] args)
        {
            Person[] persons = {new Person()} ;
            Type test1=null;
            Type test2 = null;

            List<Person> personList=new List<Person>() {new Person()};

           var enumartor= personList.GetEnumerator();

            while (enumartor.MoveNext())
            {
                Console.WriteLine(enumartor.Current);
            }

            var personType = persons.GetType();

            var personListType = personList.GetType();

            if (personType.IsArray)
                test1= typeof(IEnumerable<>).MakeGenericType(personType.GetElementType());

            if (personListType.IsGenericType)
                test2 = typeof(IEnumerable<>).MakeGenericType(personListType.GenericTypeArguments);

            Type[] ifaces = personType.GetInterfaces();

            Console.WriteLine(test1);
            Console.Read();
        }
    }
}
