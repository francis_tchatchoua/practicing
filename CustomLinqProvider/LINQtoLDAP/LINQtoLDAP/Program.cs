﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LINQtoLDAP
{
    class Program
    {

        static void Main(string[] args)
        {
            var exp = Expression.Equal(Expression.Constant(2), Expression.Constant(2));

            Console.WriteLine(exp.Type);
            Console.Read();

        }
    }

}
