﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LINQtoLDAP
{
    public class QueryableContext<TData>: IOrderedQueryable<TData>
    {
        public QueryableContext()
        {
            Provider = new QueryableProvider();
            Expression = Expression.Constant(this);
        }
        public QueryableContext(QueryableProvider provider, Expression expression)
        {
            if (provider == null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            if (!typeof(IQueryable<TData>).IsAssignableFrom(expression.Type))
            {
                throw new ArgumentOutOfRangeException(nameof(expression));
            }

            Provider = provider;
            Expression = expression;
        }
        public IEnumerator<TData> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Expression Expression { get;  }
        public Type ElementType => typeof(TData);
        public IQueryProvider Provider { get; }
    }
}
