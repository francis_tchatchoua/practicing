import 'babel-polyfill';

function* loop(n){
	for(let i=0; i<n; i++){

		yield `test ${i}`;
	}
}

function* getInputs(){

	let input1 = yield 'Francis';
	let input2 = yield `Francis ${input1}`
	yield `Francis  ${input1} ${input2}`
}



/*
let iterator = loop(6);

console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());*/

let iterator = getInputs();

console.log(iterator.next());
console.log(iterator.next('Tchatchoua'));
console.log(iterator.next('Njiki'));