
import 'babel-polyfill';

var source ={
	name:'francis',
	age:16,
	children:[{
		name:'cataleya',
		age:3,
		hobby:'football'
	},{
		name:'junior',
		age:1
	}
	]
}

var target={
	name:'francis',
	age:16,
	children:[{
		name:'cataleya',
		age:3
	},{
		name:'junior',
		age:0
	}
	]
}

Object.assign(target,source);

console.log(target);

var obj1={}
var obj2=obj1;
var result=Object.is(obj1,obj2);
console.log(result);