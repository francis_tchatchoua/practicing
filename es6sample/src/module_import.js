//import * as hello from './module_export';
import PersonService, {sayHello,age} from './module_export';

//hello.sayHello('francis');
//console.log(hello.age);

sayHello('francis');
console.log(age);
console.log(new PersonService().getName());

var source ={
	name:'francis',
	age:16,
	children:[{
		name:'cataleya',
		age:3,
		hobby:'football'
	},{
		name:'junior',
		age:1
	}
	]
}

var target={
	name:'francis',
	age:16,
	children:[{
		name:'cataleya',
		age:3
	},{
		name:'junior',
		age:0
	}
	]
}

Object.assign(target,source);

console.log(target);