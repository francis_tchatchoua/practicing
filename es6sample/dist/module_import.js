'use strict';

var _module_export = require('./module_export');

var _module_export2 = _interopRequireDefault(_module_export);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//hello.sayHello('francis');
//console.log(hello.age);

(0, _module_export.sayHello)('francis'); //import * as hello from './module_export';

console.log(_module_export.age);
console.log(new _module_export2.default().getName());

var source = {
	name: 'francis',
	age: 16,
	children: [{
		name: 'cataleya',
		age: 3,
		hobby: 'football'
	}, {
		name: 'junior',
		age: 1
	}]
};

var target = {
	name: 'francis',
	age: 16,
	children: [{
		name: 'cataleya',
		age: 3
	}, {
		name: 'junior',
		age: 0
	}]
};

Object.assign(target, source);

console.log(target);