'use strict';

require('babel-polyfill');

var _marked = [loop, getInputs].map(regeneratorRuntime.mark);

function loop(n) {
	var i;
	return regeneratorRuntime.wrap(function loop$(_context) {
		while (1) {
			switch (_context.prev = _context.next) {
				case 0:
					i = 0;

				case 1:
					if (!(i < n)) {
						_context.next = 7;
						break;
					}

					_context.next = 4;
					return 'test ' + i;

				case 4:
					i++;
					_context.next = 1;
					break;

				case 7:
				case 'end':
					return _context.stop();
			}
		}
	}, _marked[0], this);
}

function getInputs() {
	var input1, input2;
	return regeneratorRuntime.wrap(function getInputs$(_context2) {
		while (1) {
			switch (_context2.prev = _context2.next) {
				case 0:
					_context2.next = 2;
					return 'Francis';

				case 2:
					input1 = _context2.sent;
					_context2.next = 5;
					return 'Francis ' + input1;

				case 5:
					input2 = _context2.sent;
					_context2.next = 8;
					return 'Francis  ' + input1 + ' ' + input2;

				case 8:
				case 'end':
					return _context2.stop();
			}
		}
	}, _marked[1], this);
}

/*
let iterator = loop(6);

console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());*/

var iterator = getInputs();

console.log(iterator.next());
console.log(iterator.next('Tchatchoua'));
console.log(iterator.next('Njiki'));