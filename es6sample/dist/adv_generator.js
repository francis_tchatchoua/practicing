'use strict';

require('babel-polyfill');

var _marked = [process].map(regeneratorRuntime.mark);

var getFirstname = function getFirstname() {

	return new Promise(function (resolve, reject) {
		setTimeout(function () {
			return resolve('Francis');
		}, 3000);
	});
};

var getMiddleName = function getMiddleName() {
	return new Promise(function (resolve, reject) {
		setTimeout(function () {
			return resolve('Njiki');
		}, 3000);
	});
};
var getLastname = function getLastname() {
	return new Promise(function (resolve, reject) {
		setTimeout(function () {
			return resolve('Tchatchoua');
		}, 3000);
	});
};

function process() {
	return regeneratorRuntime.wrap(function process$(_context) {
		while (1) {
			switch (_context.prev = _context.next) {
				case 0:
					_context.next = 2;
					return getFirstname();

				case 2:
					_context.next = 4;
					return getMiddleName();

				case 4:
					_context.next = 6;
					return getLastname();

				case 6:
				case 'end':
					return _context.stop();
			}
		}
	}, _marked[0], this);
}

function Run() {
	var generator = process();
	var executeNext = function executeNext(result) {

		if (!result.done) {
			result.value.then(function (name) {
				console.log(name);
				names.push(name);
				executeNext(generator.next());
			});
		}
	};
	executeNext(generator.next());
}

Run();