var express= require("express");
var passport=require("passport");
var localStrategy=require('passport-local').Strategy;
var middleman=require("./middleman");
var unless=require("express-unless");

middleman.unless=unless;

var app=express();

  app.use(express.static('public'));
  app.use(passport.initialize());
  app.use(passport.session());

app.use(middleman.unless({ path: ['/login', '/'] }));

passport.use(new localStrategy(function (username,password,next) {
	
	return next(null,false,{username:username});

}));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

app.get("/login", passport.authenticate('local'), function(req,res){

	res.send("authentication successfull");
});

app.listen(3000);