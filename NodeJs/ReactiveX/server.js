var EventEmitter = require('events').EventEmitter,
    Rx = require('rx');

var source = Rx.Observable.range(0, 2)
  .map(function (x) { return Rx.Observable.range(x, 3); })
  .concatAll();

var subscription = source.subscribe(
  function (x) {
    console.log('Next: %s', x);
  },
  function (err) {
    console.log('Error: %s', err);
  },
  function () {
    console.log('Completed');
  });
