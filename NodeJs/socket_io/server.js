var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

app.get('/', function (req, res) {
  res.send('Hello patou!');
});

io.on('connection', function(socket){ 

	console.log('request received');
 });

//emit even to all connected client
setInterval(function(){
	io.sockets.emit('message', { content: 'You are connected!', importance: '1' });
},2000)


server.listen(3000,function(){
	console.log("Server listening on: http://localhost:3000");
});