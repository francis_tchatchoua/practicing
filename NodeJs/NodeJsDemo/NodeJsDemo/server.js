﻿var http = require("http");
var bodyparser = require("body-parser");

var express = require("express");
var app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));


app.post('/greet', function(req,res) {
    var data = {
        firstname: req.body.firstname, 
        lastname:req.body.lastname
    }

    res.header("Content-Type", "application/json");
    res.send(data);
    res.end();
});

var server = http.createServer(app);


server.listen(3000);

