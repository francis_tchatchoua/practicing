var mongoose = require("mongoose");
var createdModifyPlugin = require("mongoose-createdmodified").createdModifiedPlugin;
var Schema = mongoose.Schema;

var Database = function () {
    var self = this;
    self.Init = function () {

        var courseSchema = new Schema({
            title: String,
            author: String
        });
        
       var studentSchema = new Schema({
            firstname: String,
            lastname: String,
            Age: Number,
            courses: [courseSchema]
        });

        studentSchema.plugin(createdModifyPlugin);
        courseSchema.plugin(createdModifyPlugin);

        mongoose.connect('mongodb://localhost:27017/university');

        var student = mongoose.model("Student", studentSchema);
        
        var models = {}

        models.Student = student;

        return models;
    };
};

module.exports = new Database();


