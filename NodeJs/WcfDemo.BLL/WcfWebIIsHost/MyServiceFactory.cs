﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace WcfWebIIsHost
{
    public class MyServiceFactory:ServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            var host = new ServiceHost(serviceType, baseAddresses);

            return host;
        }
    }
}