﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfSelfHostDemo.CUI
{
    public class MyServiceHost<T> :ServiceHost
    {
        public MyServiceHost() :base(typeof(T))
        {
            
        }

        public MyServiceHost(params string[] baseAddresses)
            : base(typeof(T), baseAddresses.Select(a=>new Uri(a)).ToArray())
        {
            
        }

        public MyServiceHost(params Uri[] uris):base(typeof(T),uris) 
        { } 
    }
}
