﻿using System;
using System.Runtime.InteropServices;
using System.ServiceModel;
using WcfDemo.BLL;

namespace WcfSelfHostDemo.CUI
{
    class Program
    {
        static void Main(string[] args)
        {
           
            var host = new MyServiceHost<Calculation>();
            host.Open();

            Console.Read();
            
        }
    }
}
