﻿using System.ServiceModel;

namespace WcfDemo.BLL
{
    [ServiceContract(Name = "CalculationService")]
    public interface ICalculation
    {
        [OperationContract(Name = "AddNumbers")]
        int Add(int a, int b);

    }
}
