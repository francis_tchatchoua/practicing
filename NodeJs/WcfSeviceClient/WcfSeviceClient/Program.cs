﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfSeviceClient.ServiceReference1;

namespace WcfSeviceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var proxy = new CalculationServiceClient();

            Console.WriteLine(proxy.AddNumbers(2,5));
            Console.Read();
        }
    }
}
