import {expect} from 'chai';
import sequelizeFixtures from 'sequelize-fixtures';
import {File, Log, FILE, DIR} from '../src/db';
import path from 'path';

describe('File', function () {
  before(function (done) {
    Log.drop()
      .then(() => File.drop())
      .then(() => File.sync())
      .then(() => Log.sync())
      .then(() => done());
  });

  it('should return relative paths for all the files in the given directory', function(done) {
    const models = {File};
    const account = '0';
    const fixtures = [
      { model: 'File', data: { id: 1, account, parentId: null, type: DIR, name: 'level1' } },
      { model: 'File', data: { id: 2, account, parentId: 1, type: DIR, name: 'level2_1' } },
      { model: 'File', data: { id: 3, account, parentId: 1, type: DIR, name: 'level2_2' } },
      { model: 'File', data: { id: 4, account, parentId: 1, type: FILE, name: 'file2_1.txt' } },
      { model: 'File', data: { id: 5, account, parentId: 3, type: FILE, name: 'file3_1.exe' } }
    ];

    const expected = [
      {id: 1, path: 'level1'},
      {id: 2, path: path.join('level1', 'level2_1')},
      {id: 3, path: path.join('level1', 'level2_2')},
      {id: 4, path: path.join('level1', 'file2_1.txt')},
      {id: 5, path: path.join('level1', 'level2_2', 'file3_1.exe')}
    ];
    sequelizeFixtures.loadFixtures(fixtures, models).then(() => {
      File.findAll()
        .then(f => f.map(i => i.get({plain: true})))
        .then(() => {
          File.getSubTree(null, account)
            .then(subtree => subtree.map(s => ({id: s.id, path: s.path})))
            .then(subtree => expect(subtree).to.deep.equal(expected))
            .then(() => done());
        });
      });
  });

  it('delete shold mark dir/file and all children as deleted', function(done) {
    const models = {File};
    const account = '0';
    const fixtures = [
      { model: 'File', data: { id: 1, account, parentId: null, type: DIR, name: 'level1' } },
      { model: 'File', data: { id: 2, account, parentId: 1, type: DIR, name: 'level2_1' } },
      { model: 'File', data: { id: 3, account, parentId: 1, type: DIR, name: 'level2_2' } },
      { model: 'File', data: { id: 4, account, parentId: 1, type: FILE, name: 'file2_1.txt' } },
      { model: 'File', data: { id: 5, account, parentId: 3, type: FILE, name: 'file3_1.exe' } }
    ];

    sequelizeFixtures.loadFixtures(fixtures, models).then(() => {
      File.deleteSubTree(3, account)
        .then(deleted => {
          expect(deleted).to.be.have.members([3, 5]);
          expect(deleted).to.have.length(2);
        })
        .then(() => done());
    });
  });
});
