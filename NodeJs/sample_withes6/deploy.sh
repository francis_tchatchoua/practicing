#!/bin/bash
VERSION=$(node -e "console.log(require('./package.json').version)")
echo "Version $VERSION"
DEST_SERVER=$1
DEST_ENV=$2
DEST_PORT=$3
echo "Build"
docker build -t myservices/documents-api:$VERSION . 
echo "Tag as latest"
docker rmi myservices/documents-api:latest
docker tag myservices/documents-api:$VERSION myservices/documents-api:latest
echo "Copy to $DEST_SERVER, load and run"
docker save myservices/documents-api:$VERSION | bzip2 | pv |\
    ssh $DEST_SERVER "bunzip2 | docker load && docker rm -f documents-api; docker run -d --restart=always --name documents-api -p $DEST_PORT:3000 -v /data:/data -e NODE_ENV=$DEST_ENV myservices/documents-api:$VERSION"
