# My Services Documents API
## Requirements

* [Node.js](https://nodejs.org) Install latest stable release > 5.0 (tested with 5.3.0)

## Quick start

For development

```bash
npm install
npm start
```

This will start [`nodemon`](http://nodemon.io) which will restart node server each time any `js` file has changed. 
By default api starts at port 3000.

## Check code style rules
```bash
npm run validate
```

## Set up database
```bash
npm run syncdb
```
Run it only once to set up database schema from model if it was not done yet.
This script will create tables but will not update existing ones.

## Building version for production

In Order to prepare version for deployment to production environment run
```bash
npm run build
npm prune --production
```
then copy to server directories `build` and `node_modules`

## Configuration

Configuration is stored in [`src/config`](src/config) directory. Environment specific configuration is loaded based on 
`NODE_ENV` environment variable. By default configuration from `development.json` file is loaded.
Some configuration options can be overridden using environment variables. Default values and rules for configuration
are defined in [`index.js`](src/config/index.js) file.
