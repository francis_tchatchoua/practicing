import 'babel-polyfill';
import express from 'express';
import {readFileSync} from 'fs';
import http from 'http';
import https from 'https';
import cors from 'cors';
import helmet from 'helmet';
import hpp from 'hpp';
import bodyParser from 'body-parser';
import useragent from 'express-useragent';
import expressPromise from 'express-promise';
import errorHandler from 'api-error-handler';
import config from './config';
import io from './io';
import {logger, errorLogger, info} from './logging';
import routes from './routes';
import auth from './auth';

const port = config.get('port');
const useSSL = config.get('useSSL');

const app = express();
const server = useSSL ?
  https.createServer({
    pfx: readFileSync(config.get('certificate.pfx')),
    passphrase: config.get('certificate.passphrase')
  }, app) :
  http.createServer(app);

io.attach(server);
auth();

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(helmet());
app.use(hpp());
app.use(useragent.express());
app.use(expressPromise());
app.use(logger());
routes(app);
app.use(errorLogger());
app.use(errorHandler());

server.listen(port, () => {
  const address = server.address();
  info(`Custom documents API listening at http://${address.address}:${address.port}`);
});
