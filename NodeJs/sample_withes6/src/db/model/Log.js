import Sequelize from 'sequelize';
import sequelize from '../sequelize';
import File from './File';

const Log = sequelize.define('Log', {
  event: {type: Sequelize.STRING, allowNull: false},
  user: Sequelize.STRING,
  info: Sequelize.STRING
}, {
  classMethods: {
    logEvent: function (event, user, ids, info) {
      if (!Array.isArray(ids)) {
        ids = [ids];
      }
      return Log.bulkCreate(ids.map(id => ({
        event,
        user,
        info: info,
        fileId: id
      })));
    }
  }
});

Log.belongsTo(File, {
  as: 'file',
  foreignKey: {
    allowNull: false
  }
});

export default Log;
