import path from 'path';
import Sequelize from 'sequelize';
import sequelize from '../sequelize';
import createError from 'http-errors';

export const FILE = 'FILE';
export const DIR = 'DIR';
const ROOT = '__ROOT__';

function buildFilesTree (parentId, account) {
  return (function buildFilesTreeRecursive (parents) {
    const currentResult = [];
    const nextParents = {};
    let finish = true;
    return findByParentsId(Object.keys(parents), account).then(files => {
      files.forEach(f => {
        const item = f.get({plain: true});
        item.path = path.join(parents[item.parentId || ROOT], item.name);
        currentResult.push(item);
        if (f.type === DIR) {
          nextParents[item.id] = item.path;
          finish = false;
        }
      });
      if (finish) {
        return currentResult;
      }
      return buildFilesTreeRecursive (nextParents, account)
        .then(nextResult => [...currentResult, ...nextResult]);
    });
  } ({[parentId || ROOT] : ''}));
}


function findByParentsId (parents, account) {
  return File.findAll({
    where: {
      account,
      parentId: parentIdQuery()
    }
  });

  function parentIdQuery () {
    if (parents.length === 1) {
      let parentId = parents[0];
      return parentId === ROOT ? null : parentId;
    }
    return {$in: parents};
  }
}

function reduceUniqueName (existingNames) {
  const existingNamesMap = new Set(existingNames);
  return (reducing) => (result, file) => {
    let counter = 0;
    const dotIndex = file.name.lastIndexOf('.');
    const [originalName, extension] = ~dotIndex ?
      [file.name.substring(0, dotIndex), file.name.substring(dotIndex)] :
      [file.name, ''];
    let name = file.name;

    while (existingNamesMap.has(name)) {
      name = `${originalName} (${++counter})${extension}`;
    }
    existingNamesMap.add(name);
    return reducing(result, Object.assign({}, file, {name}));
  };
}

const File = sequelize.define('File', {
    name: {type: Sequelize.STRING, allowNull: false},
    type: {type: Sequelize.ENUM(FILE, DIR), allowNull: false},
    account: {type: Sequelize.UUID, allowNull: false},
    createdBy: Sequelize.STRING,
    storedFileName: Sequelize.STRING,
    fileSize: Sequelize.INTEGER
  }, {
    paranoid: true,
    defaultScope: {
      attributes: ['id', 'parentId', 'name', 'type', 'fileSize',
       'createdBy', 'updatedAt', 'createdAt']
    },
    classMethods: {
      findByParentId: (parentId, account, options) => File.findAll(Object.assign(
        {
          where: {account, parentId}
        },
        options)),
      findFile: (id, account, options) => File.findOne(Object.assign({
        where: {id, account}
      }, options)),
      addFiles: (parentId, account, files) => {
        return sequelize.transaction(t =>
          File.findByParentId(parentId, account, {transaction: t})
            .then(existingFiles => {
              const existingNames = existingFiles.map(f => f.name);
              return files.reduce(reduceUniqueName(existingNames)((result, file) => {
                result.push(Object.assign({}, file, {account, parentId}));
                return result;
              }), []);
            })
            .then(files => File.bulkCreate(files, {transaction: t}))
        );
      },
      createDir: (parentId, account, createdBy, name) => {
        return sequelize.transaction(t =>
          File.findByParentId(parentId, account, {attributes: ['name'], transaction: t})
            .then(existingFiles => {
              if (existingFiles.map(f => f.name).includes(name)) {
                throw createError.BadRequest(`Directory ${name} already exists`);
              }
              return File.create({
                account,
                createdBy,
                parentId,
                name,
                type: DIR
              }, {transaction: t});
            })
        );
      },
      rename: (fileId, account, newName) => {
        return sequelize.transaction(t => File.findFile(fileId, account, {
            transaction: t
          })
          .then(file => {
            if (!file) {
              throw new createError.NotFound(`File ${fileId} does not exist`);
            }
            return File.findByParentId(file.parentId, account,
              {attributes: ['name'], transaction: t})
              .then(existingFiles => {
                if (existingFiles.map(f => f.name).includes(newName)) {
                  throw createError.BadRequest(`${newName} already exists`);
                }
                return file.update({name: newName}, {transaction: t});
              });
          }));
      },
      deleteSubTree: (fileId, account) => {
        return buildFilesTree(fileId, account)
          .then(files => {
            const ids = [fileId, ...files.map(f => f.id)];
            return File.destroy({
              where: {id: {$in: ids}}
            }).then(() => ids);
          });
      },
      getSubTree: (fileId, account) => {
        return buildFilesTree(fileId, account);
      }
    }
  })
  ;

File.belongsTo(File, {as: 'parent'});

export default File;
