import Sequelize from 'sequelize';
import config from '../config';
import {info} from '../logging';

const connectionString = config.get('sql.connection');

export default new Sequelize(connectionString, {
  logging: info
});