export function getContentDisposition(fileName, userAgent) {
  if (userAgent.isIE && userAgent.version === '7.0' || userAgent.version === '8.0') {
    return `attachment; filename=${encodeURIComponent(fileName)}`;
  } else {
    return `attachment; filename="${fileName}"; filename*=UTF-8''${encodeURIComponent(fileName)}`;
  }
}
