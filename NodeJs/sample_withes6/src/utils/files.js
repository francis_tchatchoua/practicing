import config from '../config';
import path from 'path';

const baseDestination = config.get('upload.destination');

export function getDestination(account, file) {
  let destination = baseDestination;
  if (account) {
    destination = path.join(destination, account);
    if (file) {
      destination = path.join(destination, file);
    }
  }
  return destination;
}
