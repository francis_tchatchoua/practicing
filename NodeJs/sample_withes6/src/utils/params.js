export const mapParam = (name, mapping) => (req, res, next, value) => {
  req.params[name] = mapping(value);
  next();
};