import fs from 'fs';
import archiver from 'archiver';
import {DIR} from '../db';
import {getDestination} from './files';

export function zipFolder(subtree, account, outStream) {
  const archive = archiver.create('zip', {});
  subtree.forEach(item => {
    if (item.type === DIR) {
      archive.append(null, {
        name: item.path,
        type: 'directory'
      });
    } else {
      const filePath = getDestination(account, item.storedFileName);
      archive.append(fs.createReadStream(filePath), {name: item.path});
    }
  });
  archive.pipe(outStream);
  archive.finalize();
}
