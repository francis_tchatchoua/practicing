import express from 'express';
import passport from 'passport';
import multer from 'multer';
import fs from 'fs';
import promisify from 'es6-promisify';
import createError from 'http-errors';
import mkdirp from 'mkdirp';
import {getContentDisposition} from '../utils/contentDisposition';
import {Log, File, FILE, DIR} from '../db';
import {getDestination} from '../utils/files';
import {zipFolder} from '../utils/zip';
import {mapParam} from '../utils/params';
import io from '../io';

const readFile = promisify(fs.readFile);

const router = express.Router();

router.param('parentId', mapParam('parentId', Number));
router.param('fileId', mapParam('fileId', Number));

router.use(passport.authenticate('jwt', {session: false}));

const storage = multer.diskStorage({
  destination: (req, res, cb) => {
    const {user: {clientId}} = req;
    const destPath = getDestination(clientId);
    mkdirp(destPath, (err) => {
      cb(err, destPath);
    });
  }
});

const upload = multer({storage});

router.get('/:parentId?', (req, res) => {
  const parentId = req.params.parentId || null;
  const {user: {clientId}} = req;
  return res.json(File.findByParentId(parentId, clientId));
});

router.post('/download/:fileId', (req, res) => {
  const {params: {fileId}, user: {clientId}} = req;
  res.send(
    File.findFile(fileId, clientId, {
        attributes: ['id', 'name', 'storedFileName']
      })
      .then(file => {
        if (!file) {
          throw new createError.NotFound(`File ${fileId} does not exist`);
        }
        res.setHeader('Content-Disposition',
          getContentDisposition(file.name, req.useragent)
        );
        return getDestination(clientId, file.storedFileName);
      })
      .then(readFile)
  );
});

router.post('/download-zip/:parentId?', (req, res, next) => {
  const {params: {parentId}, user: {clientId}} = req;
  return File.getSubTree(parentId, clientId)
    .then(subtree => {
      const root = subtree.find(f => f.id === parentId);
      const fileName =  `${(root && root.fileName) || 'download'}.zip`;
      res.setHeader('Content-Disposition', getContentDisposition(fileName, req.useragent));
      zipFolder(subtree, clientId, res);
      next();
    });
});

router.post('/rename/:fileId', (req, res) => {
  const {params: {fileId}, user: {clientId, id: userId}, body: {name}} = req;
  return res.json(
    File.rename(fileId, clientId, name)
      .then(file => {
        io.to(clientId).emit('file renamed', {file, userId});
        Log.logEvent('rename', userId, file.id, `renamed to ${name}`);
        return file;
      }));
});

router.delete('/:fileId', (req, res) => {
  const {params: {fileId}, user: {clientId, id: userId}} = req;
  return res.json(File.deleteSubTree(fileId, clientId).then(ids => {
    io.to(clientId).emit('files deleted', {files: ids, userId});
    Log.logEvent('delete', userId, ids);
    return ids;
  }));
});

router.post('/upload/:parentId?', upload.array('file'), (req, res) => {
  const parentId = req.params.parentId || null;
  const {user: {clientId, id: userId, givenName}} = req;
  const files = req.files.map(file => {
    return {
      createdBy: givenName,
      name: file.originalname,
      type: FILE,
      storedFileName: file.filename,
      fileSize: file.size
    };
  });

  //IE8 requires content type text/html, otherwise it will prompt to download a file
  res.contentType('text/html');
  return res.json(
    File.addFiles(parentId, clientId, files)
      .then(records => {
        return File.findAll({
          where: {
            account: clientId,
            parentId,
            storedFileName: {$in: records.map(r => r.storedFileName)}
          }
        });
      })
      .then(files => {
        io.to(clientId).emit('files added', {files, userId});
        Log.logEvent('create', userId, files.map(f => f.id));
        return files;
      })
  );
});

router.post('/create-dir/:parentId?', (req, res) => {
  const parentId = req.params.parentId || null;
  const {user: {clientId, id: userId, givenName}, body: {name}} = req;
  return res.json(
    File.createDir(parentId, clientId, givenName, name)
      .then(dir => dir.get({plain: true}))
      .then(dir => {
        io.to(clientId).emit('files added', {files: [dir], userId});
        Log.logEvent('create', userId, dir.id);
        return dir;
      })
  );
});

router.post('/move/:fileId', (req, res) => {
  const {user: {clientId, id: userId}, params: {fileId}, body: {dest}} = req;
  return res.json(
    File.findAll({
        where: {
          account: clientId,
          id: {$in: [fileId, dest]}
        },
        limit: 2
      })
      .then(files => {
        var filesById = files.reduce((result, file) => Object.assign(
          result,
          {[file.id]: file}), {});
        const {[fileId]: file, [dest]: destDirectory} = filesById;
        if (!file) {
          throw new createError.BadRequest(`From file ${fileId} does not exist`);
        }
        if (!destDirectory) {
          throw new createError.BadRequest(`Destination ${dest} does not exist`);
        }
        if (destDirectory.type !== DIR) {
          throw new createError.BadRequest(`${dest} is not a directory`);
        }

        return file.setParent(destDirectory)
          .then(file => {
            io.to(clientId).emit('file moved', {file, userId});
            Log.logEvent('move', userId, file.id);
            return file;
          });
      })
  );
});

export default router;
