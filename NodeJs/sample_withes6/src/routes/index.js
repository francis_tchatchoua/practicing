import files from './files';

export default function (app) {
  app.use('/files', files);
  app.get('/healthcheck', (req, res) => {
    res.send('OK');
  });
}
