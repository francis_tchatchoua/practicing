import convict from 'convict';
import path from 'path';

const config = convict({
  env: {
    doc: 'The applicaton environment.',
    format: ['production', 'development', 'test', 'integration'],
    default: 'development',
    env: 'NODE_ENV'
  },
  oauth2: {
    secret: {
      doc: 'oauth2 secret key',
      default: 'M3+kWzX6fZkOrAm/9JQ63WRsXH9F6qURTVS1wNDmqRI='
    }
  },
  sql: {
    connection: {
      doc: 'Connection string to the database.',
      format: String,
      default: '',
      env: 'DOCUMENTS_DB'
    }
  },
  certificate: {
    pfx: {
      doc: 'Path to pfx certificate file',
      format: String,
      default: './certificate/interoutenew.pfx'
    },
    passphrase: {
      doc: '',
      format: String,
      default: 'Interoute1'
    }
  },
  useSSL: {
    doc: 'Use SSL',
    format: Boolean,
    default: false,
    env: 'USE_SSL'
  },
  port: {
    doc: 'The port to bind.',
    format: 'port',
    default: 0,
    env: 'PORT'
  },
  upload: {
    limits: {
      doc: 'Limits (multer doc)',
      format: Object,
      default: {}
    },
    'destination': {
      doc: 'Path for uploaded files',
      format: String,
      default: '.'
    }
  },
  logging: {
    directory: {
      format: String,
      default: 'logs'
    },
    logs: {
      filename: {
        doc: 'File name for default logger',
        format: String,
        default: 'logs.log'
      }
    },
    requests: {
      filename: {
        format: String,
        default: 'requests.log'
      }
    },
    errors: {
      filename: {
        format: String,
        default: 'errors.log'
      }
    }
  }
});

const filePath = path.join(__dirname, `${config.get('env')}.json`);

config.loadFile(filePath);
config.validate({strict: true});

export default config;
