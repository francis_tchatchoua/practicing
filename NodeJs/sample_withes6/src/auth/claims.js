export const interouteAdminClaimKey = 'http://claims.interoute.com/InterouteAdmin';
export const clientIdClaimKey = 'http://claims.interoute.com/CurrentClientId';
export const userIdClaimKey = 'http://claims.interoute.com/UserId';
export const givenNameClaimKey = 'given_name';