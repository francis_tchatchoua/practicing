import passport from 'passport';
import {Strategy as JwtStrategy} from 'passport-jwt';
import config from '../config';
import * as claims from './claims';

export const secretKey = new Buffer(config.get('oauth2.secret'), 'base64');

export function mapClaimsToUser(jwtPayload) {
  return {
    isInterouteAdmin: (jwtPayload[claims.interouteAdminClaimKey] === 'true'),
    givenName: jwtPayload[claims.givenNameClaimKey],
    clientId: jwtPayload[claims.clientIdClaimKey],
    id: jwtPayload[claims.userIdClaimKey]
  };
}

export default function () {
  passport.use(new JwtStrategy({
    secretOrKey: secretKey,
    authScheme: 'Bearer',
    passReqToCallback: true
  }, (req, jwtPayload, done) => {
    const user = mapClaimsToUser(jwtPayload);
    done(null, user);
  }));
}
