import winston from 'winston';
import expressWinston from 'express-winston';
import config from '../config';
import path from 'path';
import mkdirp from 'mkdirp';

const logDirectory = config.get('logging.directory');

mkdirp.sync(logDirectory);
winston.level = 'verbose';
winston.add(winston.transports.File, {
  filename: path.join(logDirectory, config.get('logging.logs.filename'))
});

export const {debug, info, warn, error} = winston;

export const logger = () => expressWinston.logger({
  transports: [
    new winston.transports.Console({
      json: true,
      colorize: true
    }),
    new winston.transports.File({
      filename: path.join(logDirectory, config.get('logging.requests.filename'))
    })
  ]
});

export const errorLogger = () => expressWinston.errorLogger({
  transports: [
    new winston.transports.Console({
      json: true,
      colorize: true
    }),
    new winston.transports.File({
      filename: path.join(logDirectory, config.get('logging.errors.filename'))
    })
  ]
});