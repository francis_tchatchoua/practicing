import socketIo from 'socket.io';
import {authorize} from 'socketio-jwt';
import {mapClaimsToUser, secretKey as secret} from './auth';
import {info} from './logging';

const io = socketIo();

io.sockets
  .on('connection', authorize({secret}))
  .on('authenticated', socket => {
    const user = mapClaimsToUser(socket.decoded_token);
    info(`user user ${user.givenName} (${user.id}) connected to ${user.clientId}`);
    socket.join(user.clientId);
  });

export default io;