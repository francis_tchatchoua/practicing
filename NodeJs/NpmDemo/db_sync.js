var Sequelize = require('sequelize');

var sequelize = new Sequelize('mssql://sa:Interoute.1@localhost/SequelizeDb');

var User = sequelize.define('user', {
  firstName: {
    type: Sequelize.STRING,
    field: 'first_name'
  },
  lastName: {
    type: Sequelize.STRING
  }
}, {
  freezeTableName: true,
  timestamps: true 
});
/*
User.sync({force: true}).then(function () {
  return User.create({
    firstName: 'francis',
    lastName: 'tchatchoua'
  });
});*/

User.findOne().then(function (user) {
    console.log(user.lastName);
});