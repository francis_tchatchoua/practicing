﻿namespace ClrViaCsharpDemo
{
    public class OverLoaded
    {
        public string Name { get; set; }
        public static OverLoaded operator +(OverLoaded first,OverLoaded second)
        {
            return new OverLoaded() {Name = first.Name+" "+second.Name};
        }
    }
}
