﻿using System;

namespace ClrViaCsharpDemo
{
    public static class StaticClass
    {
        public static void AStaticMethod() { }
        public static String AStaticProperty
        {
            get { return s_AStaticField; }
            set { s_AStaticField = value; }
        }
        private static String s_AStaticField;
        public static event EventHandler AStaticEvent;
    }
}
