﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ClrViaCsharpDemo
{
    class Program
    {
        public static void Main()
        {
           dynamic stringType = new StaticMemberDynamicWrapper(typeof(string));

            var results = stringType.Concat("A", "B");

            Console.WriteLine(results);

            var first = new OverLoaded() {Name = "first"};
            var second = new OverLoaded() {Name = "second"};

            var res = first + second;

            Console.WriteLine(res.Name);

            Convertion con = 123;

            Console.WriteLine(con.ToString());

            var con2 = (int) new Convertion {Num = 8292992};

            Console.WriteLine(con2.ToString());

            Console.Read();


            var test1 = null;
            string test2 = null;

        }
      
    }


}
