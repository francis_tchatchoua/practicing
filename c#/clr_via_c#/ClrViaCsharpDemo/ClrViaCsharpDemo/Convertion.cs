﻿using System;

namespace ClrViaCsharpDemo
{
    public  class Convertion
    {
        public Int32 Num { get; set; }
        public static  implicit operator Convertion(Int32 num)
        {
            return new Convertion() {Num = num};
        }

        public static explicit operator Int32(Convertion con)
        {
            return con.Num;
        }

        public override string ToString()
        {
            return Num.ToString();
        }
    }
}
