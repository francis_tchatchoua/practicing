﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicVisitor
{
    class Program
    {
        static void Main(string[] args)
        {
            var math = new AddExpression(new DoubleExpression(5), new AddExpression(new DoubleExpression(2), new DoubleExpression(3)));
            var sb= new StringBuilder();
            math.Print(sb);

            Console.WriteLine(sb);
        }
    }
}
