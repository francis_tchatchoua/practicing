﻿using System.Media;
using System.Text;

namespace BasicVisitor
{
    public class DoubleExpression:Expression
    {
        readonly double _number;
        public DoubleExpression(double number)
        {
            _number = number;
        }
        public override void Print(StringBuilder sb)
        {
            sb.Append(_number);
        }
    }
}
