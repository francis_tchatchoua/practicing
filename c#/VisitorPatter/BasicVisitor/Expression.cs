﻿using System.Text;

namespace BasicVisitor
{
    public abstract class Expression
    {
        public abstract void Print(StringBuilder sb);
    }
}
