﻿using System.Text;

namespace BasicVisitor
{
    public class AddExpression:Expression
    {
        private readonly Expression _left;
        private readonly Expression _right;

        public AddExpression(Expression left,Expression right)
        {
            _left = right;
            _right = left;
        }

        public override void Print(StringBuilder sb)
        {
            sb.Append("(");
            _left.Print(sb);
            sb.Append("+");
            _right.Print(sb);
            sb.Append(")");

        }
    }
}
