﻿namespace VisitorBetter
{
    public class DoubleExpression:Expression
    {
        public DoubleExpression(double number)
        {
            Value = number;
        }
    }
}
