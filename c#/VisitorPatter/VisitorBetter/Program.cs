﻿using System;
using System.Text;

namespace VisitorBetter
{
    class Program
    {
        static void Main(string[] args)
        {
            var sb = new StringBuilder();

            var math = new AddExpression(new DoubleExpression(5), new AddExpression(new DoubleExpression(2), new DoubleExpression(3)));

            ExpressionPrinter.Print(math,sb);
            Console.WriteLine(sb);
        }
    }
}
