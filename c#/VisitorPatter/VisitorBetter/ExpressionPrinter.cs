﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VisitorBetter
{
    using DicType = Dictionary<Type, Action<Expression, StringBuilder>>;
    public  class ExpressionPrinter
    {
        private  static DicType actions =new  DicType()
        {
           [typeof(DoubleExpression)]=(e,sb)=> sb.Append(e.Value),
           [typeof(AddExpression)]= (e, sb) =>
           {
               var ae = (AddExpression) e;
               sb.Append("(");
               Print(ae.Left, sb);
               sb.Append("+");
               Print(ae.Right, sb);
               sb.Append(")");
           }
            
        };

        public static void Print(Expression e, StringBuilder sb)
        {
            actions[e.GetType()](e, sb);
        }
    }
}
