﻿namespace VisitorBetter
{
    public abstract class Expression
    {
        public double Value { get; set; }
    }
}
