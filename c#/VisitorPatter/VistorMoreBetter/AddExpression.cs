﻿namespace VistorMoreBetter
{
    public class AddExpression:Expression
    {
        public Expression Left;
        public Expression Right;

        public AddExpression(Expression left,Expression right)
        {
            Left = left;
            Right = right;
        }

        public override void Accept(IExpressionVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
