﻿using System;
using System.Text;

namespace VistorMoreBetter
{
    public class PrintExpressionVisitor:IExpressionVisitor
    {
        StringBuilder sb=new StringBuilder();
        public void Visit(AddExpression ae)
        {
            sb.Append("(");
            ae.Left.Accept(this);
            sb.Append("+");
            ae.Right.Accept(this);
            sb.Append(")");
        }

        public void Visit(DoubleExpression de)
        {
            sb.Append(de.Value);
        }

        public override string ToString()
        {
            return sb.ToString();
        }
    }
}
