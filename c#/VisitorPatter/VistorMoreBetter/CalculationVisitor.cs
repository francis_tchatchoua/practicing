﻿namespace VistorMoreBetter
{
    public class CalculationVisitor:IExpressionVisitor
    {
         double _result;
        public void Visit(AddExpression ae)
        {
            ae.Left.Accept(this);
            var a = _result;

            ae.Right.Accept(this);

            var b = _result;

            _result = a + b;
        }

        public void Visit(DoubleExpression de)
        {
            _result = de.Value;
        }

        public override string ToString()
        {
            return $"{_result}";
        }
    }
}