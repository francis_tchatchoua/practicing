﻿using System;

namespace VistorMoreBetter
{
    class Program
    {
        static void Main(string[] args)
        {
            var math = new AddExpression(new DoubleExpression(5), 
                new AddExpression(new DoubleExpression(2), new DoubleExpression(3)));


            var visitor = new PrintExpressionVisitor();
            var calVisitor = new CalculationVisitor();

            visitor.Visit(math);
            calVisitor.Visit(math);

            Console.WriteLine($"{visitor} = {calVisitor}");
        }
    }
}
