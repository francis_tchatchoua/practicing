﻿using System.Data;

namespace VistorMoreBetter
{
    public abstract class Expression
    {
        public abstract void Accept(IExpressionVisitor visitor);
    }
}
