﻿using System.Collections.Generic;

namespace ExpressionTreesDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var product= new Product() {Price = 2, Discount = new List<decimal>() {1,2,3,4,5,6,7,8,9,10},Trainers = new List<Trainer>()
            {
                new Trainer() {Price = 14},
                new Trainer() {Price = 23},
                new Trainer() {Price = 35}
            } };

            var converter = new CurrencyConverter<Product>().Configure();

            converter.Prop(p => p.Price);
            converter.List(p => p.Discount);
            converter.ForEach(p => p.Trainers, conf => conf.Prop(t => t.Price));

            converter.Apply(product,10);

            
        }
    }
}
