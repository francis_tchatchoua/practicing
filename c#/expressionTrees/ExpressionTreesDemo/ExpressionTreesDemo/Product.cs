﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionTreesDemo
{
    public class Product
    {
        public decimal Price { get; set; }
        public List<decimal> Discount { get; set; }

        public List<Trainer> Trainers { get; set; }
    }

    public class Trainer
    {
        public decimal Price { get; set; }
    }
}
