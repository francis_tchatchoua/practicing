﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ExpressionTreesDemo
{
    public class CurrencyConverter<TMain>
    {
        private CurrencyCoverterConfig<TMain> _configuration;
        public void Apply(TMain product,decimal factor)
        {
            _configuration.Apply(product,factor);
        }
        public CurrencyCoverterConfig<TMain> Configure()
        {
            _configuration= new CurrencyCoverterConfig<TMain>();
            return _configuration;
        }

        public class CurrencyCoverterConfig<T>
        {
            public CurrencyCoverterConfig<T> Prop(Expression<Func<T,decimal>> expression)
            {
                var getter = expression.Compile();
                var member = (MemberExpression)expression.Body;
                var param = Expression.Parameter(typeof(decimal), "value");

                var setterExp = Expression.Lambda<Action<T,decimal>>(Expression.Assign(member, param), expression.Parameters[0], param);

                var setter = setterExp.Compile();
                _items.Add(new PropertyItem<T>() {Getter = getter,Setter = setter});
                return this;
            }

            public CurrencyCoverterConfig<T> List(Expression<Func<T,List<decimal>>> expression)
            {
                var getter = expression.Compile();
                var member = (MemberExpression) expression.Body;
                var param = Expression.Parameter(typeof(List<decimal>), "value");
                var setterExp = Expression.Lambda<Action<T,List<decimal>>>(Expression.Assign(member, param), expression.Parameters[0],param);

                var setter = setterExp.Compile();
                var item = new ListItem<T>() {Getter = getter, Setter = setter};
                _items.Add(item);
                return this;
            }

            public CurrencyCoverterConfig<T> ForEach<TItem>(Func<T, List<TItem>> collectionFunc,
                Action<CurrencyCoverterConfig<TItem>> configuration)

            {
                var subConfig = new CurrencyCoverterConfig<TItem>();
                configuration(subConfig);
                var item = new SubConfigItem<T, TItem>
                {
                    SubConfig = subConfig,
                    Accessor = collectionFunc
                };
                _items.Add(item);
                return this;
            }
            public void Apply(T product, decimal factor)
            {
                foreach (var item in _items)
                {
                    item.Apply(product,factor);
                }

            }

            List<ICurrencyConverterNode<T>> _items= new List<ICurrencyConverterNode<T>>();
        }


        private interface ICurrencyConverterNode<T>
        {
            void Apply(T root,decimal factor);
        }

        private class PropertyItem<T> : ICurrencyConverterNode<T>
        {
            public Func<T, decimal> Getter { get; set; }
            public Action<T,decimal> Setter { get; set; }

            public void Apply(T root,decimal factor)
            {
                var val = Getter(root);
                var newValue = val * factor;

                Setter(root,newValue);
            }
        }

        private class ListItem<T> : ICurrencyConverterNode<T>
        {
            public Func<T, List<decimal>> Getter { get; set; }
            public Action<T, List<decimal>> Setter { get; set; }
            public void Apply(T root, decimal factor)
            {
                var value = Getter(root);
                var newValue = value.Select(x => x * factor).ToList();

                Setter(root, newValue);
            }
        }
        private class SubConfigItem<T,TItem> : ICurrencyConverterNode<T>
        {
            public Func<T, List<TItem>> Accessor { get; set; }
            public CurrencyCoverterConfig<TItem> SubConfig{ get; set; }
            
            public void Apply(T root, decimal factor)
            {
                var val = Accessor(root);
                if (val == null)
                    return;
                foreach (var item in val)
                {
                    SubConfig.Apply(item, factor);
                }
            }
        }

    }
}
