﻿using System;
using System.Collections.Generic;
using System.Reflection;
using SampleAssemblyV1;

namespace ReflectionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //var sampleClass = Activator.CreateInstance(typeof (SampleClass));


            //var sampleType = typeof (SampleClass);

            //var cacheField = sampleType.GetField("cachedItems", BindingFlags.NonPublic | BindingFlags.Instance);
            //if (cacheField != null)
            //{
            //    var items = cacheField.GetValue(sampleClass) as List<string>;

            //    foreach (var item in items)
            //    {
            //        Console.WriteLine(item);
            //    }
            //}

            IDataReopository reopository=new MySqlRepository();
            


            var service = new UserService(reopository);

            var users = service.GetAllUsers();

            foreach (var u in users)
            {
                Console.WriteLine("Firstname:{0}, Lastname:{1}",u.FirstName,u.LasrName);
            }
            Console.Read();
        }

    }
}
