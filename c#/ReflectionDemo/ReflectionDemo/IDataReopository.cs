﻿using System.Collections.Generic;

namespace ReflectionDemo
{
    public interface IDataReopository
    {
        
        void Create(User u);
        bool Save(User u);
        User Find(string id);
        List<User> FindAll();
    }

    public class SqlRepository:IDataReopository
    {

        public void Create(User u)
        {
            throw new System.NotImplementedException();
        }

        public bool Save(User u)
        {
            throw new System.NotImplementedException();
        }

        public User Find(string id)
        {
            throw new System.NotImplementedException();
        }

        public List<User> FindAll()
        {
            return new List<User>
            {
               new User(){FirstName = "joanna", LasrName = "czchowska"},
               new User(){FirstName = "didier",LasrName = "njiki"}
            };
        }
    }

    public class MySqlRepository:IDataReopository
    {

        public void Create(User u)
        {
            throw new System.NotImplementedException();
        }

        public bool Save(User u)
        {
            throw new System.NotImplementedException();
        }

        public User Find(string id)
        {
            throw new System.NotImplementedException();
        }

        public List<User> FindAll()
        {
            return new List<User>
            {
               new User(){FirstName = "francis", LasrName = "tchatchoua"},
               new User(){FirstName = "thiery",LasrName = "Tounouga"}
            };
        }
    }
}
