﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionDemo
{
    public class UserService
    {
        private readonly IDataReopository _reopository;
        public UserService(IDataReopository reopository)
        {
            _reopository = reopository;
        }

        public List<User> GetAllUsers()
        {
            return _reopository.FindAll();
        } 
    }   
}
