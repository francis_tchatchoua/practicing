﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleAssemblyV2
{
    public interface IPersonRepository
    {
        void AddPerson(Person person);
        IEnumerable<Person> GetPeople();
        Person GetPerson(string name);
        void UpdatePesron(string name, Person person);
        void DeletePesron(string name);
    }

    public class PersonRepository : IPersonRepository
    {

        public void AddPerson(Person person)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Person> GetPeople()
        {
            throw new NotImplementedException();
        }

        public Person GetPerson(string name)
        {
            throw new NotImplementedException();
        }

        public void UpdatePesron(string name, Person person)
        {
            throw new NotImplementedException();
        }

        public void DeletePesron(string name)
        {
            throw new NotImplementedException();
        }
    }
}
