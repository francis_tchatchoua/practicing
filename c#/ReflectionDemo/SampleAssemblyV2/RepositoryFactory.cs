﻿namespace SampleAssemblyV2
{
    public class RepositoryFactory
    {
        public static IPersonRepository GetPersonRepository()
        {
            return new PersonRepository();
        }
    }
}
