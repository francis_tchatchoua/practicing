﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class CurrencyConverter<TMain> : ICurrencyConverter<TMain>
    {
        private CurrencyConverterConfig<TMain> _configuration;
        public void Apply(TMain root, decimal factor)
        {
            _configuration.Apply(root, factor);
        }

        public ICurrencyConverterConfig<TMain> Configure()
        {
            _configuration = new CurrencyConverterConfig<TMain>();
            return _configuration;
        }
    }

    public interface ICurrencyConverter<T>:ICurrencyConverterNode<T>
    {
        ICurrencyConverterConfig<T> Configure();
    }

    public class CurrencyConverterConfig<T>: ICurrencyConverterConfig<T>
    {
        public ICurrencyConverterConfig<T> Prop(Expression<Func<T, decimal>> expression)
        {
            var getter = expression.Compile();
            var member = (MemberExpression) expression.Body;
            var param = Expression.Parameter(typeof (decimal), "value");
            var setter = Expression.Lambda<Action<T, decimal>>(
                Expression.Assign(member, param), expression.Parameters[0], param).Compile();

            var propertyItem = new PropertyItem<T>() {Getter= getter, Setter = setter};
                _items.Add(propertyItem);

            return this;
        }
        private readonly List<ICurrencyConverterNode<T>> _items = new List<ICurrencyConverterNode<T>>();

        public void Apply(T root, decimal factor)
        {
            if (root == null) throw new ArgumentNullException(nameof(root));
            _items.ForEach(p => p.Apply(root, factor));
        }
    }

    public interface ICurrencyConverterConfig<T>
    {
        ICurrencyConverterConfig<T> Prop(Expression<Func<T, decimal>> expression);
    }


    public interface ICurrencyConverterNode<T>
    {
        void Apply(T root, decimal factor);
    }
    public class PropertyItem<T> : ICurrencyConverterNode<T>
    {
        public Func<T, decimal> Getter { get; set; }
        public Action<T, decimal> Setter { get; set; }

        public void Apply(T root, decimal factor)
        {
            try
            {
                var val = Getter(root);
                var newVal = val * factor;
                Setter(root, newVal);
            }
            catch (NullReferenceException) { }
        }
    }
}
