﻿using System;

namespace ConsoleApplication1
{
    [AttributeUsage(AttributeTargets.Property)]
    public  class CurrencyConverterAttribute:Attribute
    {}
}
