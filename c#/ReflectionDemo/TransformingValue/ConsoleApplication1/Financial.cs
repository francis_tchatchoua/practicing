﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    
    public class Financial
    {
        [CurrencyConverter]
        public decimal MarketCap { get; set; }
        [CurrencyConverter]
        public decimal Revenue { get; set; }
        public List<Metric> Metrics { get; set; }

        public override string ToString()
        {
            return $"MarketCap:{MarketCap} Revenue:{Revenue}";
        }
    }

    public class Metric
    {
        public int Id { get; set; }
        [CurrencyConverter]
        public decimal Value { get; set; }
    }
}
