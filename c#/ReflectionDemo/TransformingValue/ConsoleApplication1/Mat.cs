﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public  class Mat <T>
    {
        private readonly T _data;

        public Mat(T data)
        {
            _data = data;
        }
        
        public override string ToString()
        {
            return _data.ToString();
        }
    }
}
