﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace structureMapDemo
{
    public interface IPerson
    {
        string GetName();
    }

    public class Person : IPerson
    {
        public string Name = "patou";
        public string GetName()
        {
            return "structure map";
        }
    }
}
