﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using StructureMap;

namespace structureMapDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var person =new Person() {Name = "pat"};

            string str = "Test";
            PrintProperty(() => person.Name);
            Console.Read();
        }

        public static void PrintProperty<T>(Expression<Func<T>> e)
        {

            /* var member = (MemberExpression)e.Body;
             Console.WriteLine(member);
             string propertyName = member.Member.Name;
             var parameter = Expression.Parameter(typeof (string), "newName");

             var lambexp = Expression.Lambda<Action<string>>(Expression.Assign(member, parameter), parameter);
             var setter = lambexp.Compile();
             setter("new patou");
             T value = e.Compile()();
             Console.WriteLine("{0} : {1}", propertyName, value);*/

            AdditionLambda();
            Console.Read();
        }

        public  static void AdditionLambda()
        {
            var person = new Person() {Name = "tamo"};

            var memb = Expression.Field(Expression.Constant(person), "Name");

            var num1 = Expression.Parameter(typeof (int),"num1");
            var num2 = Expression.Parameter(typeof(int), "num2");

            var lamd = Expression.Lambda<Func<int,int,int>>(Expression.Add(num1, num2), num1, num2);

            var res = lamd.Compile();

            var memberExp = Expression.Lambda<Func<string>>(memb);

            Console.WriteLine(memberExp.Compile()());

            Console.WriteLine(res(6,8));

        }
    }
}
