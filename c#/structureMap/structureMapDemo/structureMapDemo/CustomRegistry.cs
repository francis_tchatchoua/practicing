﻿using StructureMap;

namespace structureMapDemo
{
    public class CustomRegistry:Registry
    {
        public CustomRegistry()
        {
            For<IPerson>().Use<Person>();
        }
    }
}
