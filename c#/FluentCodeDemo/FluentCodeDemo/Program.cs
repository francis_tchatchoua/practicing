﻿using System;

namespace FluentCodeDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var personDto = new PersonDto()
            {
                FirstName = "patou"
            };

            
            var mapper= new FluentMapper<Person,PersonDto>();

            mapper.ForMember(p => p.Prenom, pd => pd.FirstName);

            var result = mapper.Map(personDto);

            Console.WriteLine(result.ToString());
            Console.Read();
        }
    }
}
