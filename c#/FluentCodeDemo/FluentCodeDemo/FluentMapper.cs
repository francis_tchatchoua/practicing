﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace FluentCodeDemo
{
    public class FluentMapper<TTarget, TSource> where TTarget:new()
    {
        private static readonly TTarget TargetObj= new TTarget();
        public FluentMapper<TTarget, TSource> ForMember(Expression<Func<TTarget, object>> targetExpression, 
            Expression<Func<TSource, object>> sourceExpression)
        {
            var getter = sourceExpression.Compile();

            var targetMember = (MemberExpression)targetExpression.Body;

            var fi = targetMember.Member.MemberType;

            var sourceParam = Expression.Parameter(typeof(object), "source");
            var assign = Expression.Lambda<Action<TTarget,object>>(Expression.Assign(targetMember, sourceParam), 
                targetExpression.Parameters[0],
                sourceParam).Compile();

            var context = new MapContext()
            {
                Getter = getter,
                Setter = assign
            };

            _mapped.Add(context);

            return this;
        }

        public TTarget Map(TSource source)
        {
            foreach (var map in _mapped)
            {
                map.Apply(source);
            }

            return TargetObj;
        }

        private readonly List<MapContext> _mapped = new List<MapContext>(); 

        private class MapContext
        {

            public Func<TSource, object> Getter { private get; set; }
            public Action<TTarget, object> Setter { private get; set; }

            public void Apply(TSource source)
            {
                var value = Getter(source);
                Setter(TargetObj, value);
            }

        }
    }
    
}