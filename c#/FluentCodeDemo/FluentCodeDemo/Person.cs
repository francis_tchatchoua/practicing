﻿using NUnit.Framework.Constraints;

namespace FluentCodeDemo
{
    public class Person
    {
        public string Prenom { get; set; }

        public override string ToString()
        {
            return $"Ton prenom cest: {Prenom}";
        }
    }
}