﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
            Test1();
        }

        public static void Test1()
        {
            var term = 'a';
            var testString = "hdhdj hhhhhkdkkdddaakkdaaaaaaaaaaaa ldj hjjj aaaaaaaa ggdg";

            var regex= new Regex($"[{term}]+");

            var max = regex.Matches(testString)
                      .Cast<Match>()
                      .Select(m => m.Value.Length)
                      .Max();

            Console.WriteLine(max);
            Console.Read();
        }
        public static void Test()
        {
            var test = new List<int> { 7, 9, 6, 4, 6, 5, 8 };

            var sum = 13;

            var complement = new Dictionary<int, int>();

            var results = new List<string>();

            foreach (var number in test.Select((x, i) => new { index = i, value = x }))
            {
                if (complement.ContainsKey(sum - number.value))
                {
                    results.Add($"{sum - number.value},{number.value}");
                }

                if (!complement.ContainsKey(number.value))
                {
                    complement[number.value] = number.index;
                }
                
            }
            foreach (var r in results)
            {
                Console.WriteLine(r);
            }
            Console.Read();
        }
    }
}
