﻿using System;
using System.IO;

namespace DemoApp
{
    public interface IVesselRepository
    {
        void Add(Vessel data);
    }

    public class VesselRepository: IVesselRepository
    {
        public void Add(Vessel data)
        {
            throw new NotImplementedException();
        }
    }

    public class Vessel
    {
        public int Id { get; set; }
    }

    public interface IVesselService
    {
        void Add(Vessel vessel);

    }

    public interface IFilePathProvider
    {
        string GetLogFilePath();
    }
    public interface ILogSevice
    {
        void LogMessage(string message);
    }

    public class LogSevice: ILogSevice
    {
        private readonly IFilePathProvider _filePathProvider;

        public LogSevice(IFilePathProvider filePathProvider)
        {
            _filePathProvider = filePathProvider;
        }
        public void LogMessage(string message)
        {
            var path = _filePathProvider.GetLogFilePath();
            File.WriteAllText(path,message);
        }
    }
    public class VesselService: IVesselService
    {
        private readonly IVesselRepository _vesselRepository;
        private readonly ILogSevice _logSevice;

        public VesselService(IVesselRepository vesselRepository,ILogSevice logSevice)
        {
            _logSevice = logSevice;
            _vesselRepository = vesselRepository;
        }
        public void Add(Vessel vessel)
        {
            _vesselRepository.Add(vessel);

            var message = $"Vessel Added {vessel.Id}";

            _logSevice.LogMessage(message);

        }
    }
}
