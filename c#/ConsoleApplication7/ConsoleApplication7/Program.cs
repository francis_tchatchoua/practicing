﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApplication7
{
    class Program
    {
        static int Solution(string S)
        {
            var regex=new Regex("[A-Z]+[^0-9]*");

            var matchesResult = regex.Matches(S);

            var matches = matchesResult.Cast<Match>().ToList();

            if (!matches.Any()) return -1;


            return matches.Max(m => m.Value.Length);
        }
        static void Main(string[] args)
        {
            var result = Solution("a0bb");

            Console.WriteLine("length is: "+result);
            Console.Read();
        }
    }
}
