﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class MyClass
    {
        public int Test { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var myClass1 = new MyClass() {Test = 2};
            var myclass2 = new MyClass() {Test = 9};
        }
    }
}
