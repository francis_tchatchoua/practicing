﻿using System.Linq;
using System.Security.Claims;

namespace ClaimDemoApp.BLL
{
    public class CustomAuthorisationManager : ClaimsAuthorizationManager
    {
        public override bool CheckAccess(AuthorizationContext context)
        {
            string resource = context.Resource.First().Value;
            string action = context.Action.First().Value;

            if (action == "Show" && resource == "Code")
            {
                var likeCsharp = context.Principal.HasClaim("http://mywebsite.com/likeprogramming", "c#");
                return likeCsharp;
            }

            return false;
        }
    }
}
