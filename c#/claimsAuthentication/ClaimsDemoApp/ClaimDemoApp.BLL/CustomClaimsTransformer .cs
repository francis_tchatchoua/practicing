﻿using System.Collections.Generic;
using System.Security;
using System.Security.Claims;

namespace ClaimDemoApp.BLL
{
    public class CustomClaimsTransformer : ClaimsAuthenticationManager
    {
        public override ClaimsPrincipal Authenticate(string resourceName, ClaimsPrincipal incomingPrincipal)
        {
            if (incomingPrincipal.Identity.Name == null) throw new SecurityException("A user with no name???");

            var nameClaim = incomingPrincipal.FindFirst(ClaimTypes.Name);

            var claims = new List<Claim> { nameClaim,new Claim("http://mywebsite.com/likeprogramming","c#") };

            return new ClaimsPrincipal(new ClaimsIdentity(claims,"custom"));
        }
    }
}
