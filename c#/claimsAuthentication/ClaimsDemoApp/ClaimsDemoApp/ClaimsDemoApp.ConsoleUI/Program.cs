﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.Linq;
using System.Security.Claims;
using System.Security.Permissions;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClaimDemoApp.BLL;

namespace ClaimsDemoApp.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {

            SetUpClaim();
            UseClaim();

            
        }

        private static void UseClaim()
        {
            var principal = Thread.CurrentPrincipal as ClaimsPrincipal;

            var newPrincipal = (new CustomClaimsTransformer()).Authenticate("", principal);

            var name = newPrincipal.FindFirst(ClaimTypes.Name).Value;

            Console.WriteLine(name);
            Console.WriteLine("is authenticated: {0}", newPrincipal.Identity.IsAuthenticated);
            //ClaimsPrincipalPermission.CheckAccess("Code", "Show");
            ShowMeTheCode();
            Console.Read();
        }

        private static void SetUpClaim()
        {
            var incomingPrincipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());

            Thread.CurrentPrincipal = FederatedAuthentication.FederationConfiguration.IdentityConfiguration
                .ClaimsAuthenticationManager.Authenticate("none", incomingPrincipal);
        }
        [ClaimsPrincipalPermission(SecurityAction.Demand, Operation = "Show", Resource = "Code")]
        private static void ShowMeTheCode()
        {
            Console.WriteLine("see the code");
        }

        private  static int Calculate(int a, int b)
        {
            return a + b;
        }
    }
}
