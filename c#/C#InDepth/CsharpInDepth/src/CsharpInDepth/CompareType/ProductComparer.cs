﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CsharpInDepth.CompareType
{
    public class ProductComparer:IComparer<Product>
    {
        public int Compare(Product x, Product y)
        {
            return string.Compare(x.Name, y.Name, StringComparison.Ordinal);
        }
    }
}
