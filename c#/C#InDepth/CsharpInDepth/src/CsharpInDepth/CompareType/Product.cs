﻿using System.Collections.Generic;

namespace CsharpInDepth.CompareType
{
    public class Product
    {
        public string Name { get; private set; }
        public double Price { get; private set; }
        public Product(string name, double price)
        {
            Name = name;
            Price = price;
        }

        public List<Product> GetSampleProducts()
        {
            return new List<Product>()
            {
                new Product(name:"train",price:20000),
                new Product(name:"dable",price:100.0),
                new Product(name:"car",price:970000)
            };
        } 

        public override string ToString()
        {
            return $"Name: {Name}, Price: {Price}";
        }
    }
}
