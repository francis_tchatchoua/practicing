﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CsharpInDepth.CompareType;

namespace CsharpInDepth
{
    public class Program
    {
        public static void Main(string[] args)
        {
           var products = new Product("test",1).GetSampleProducts();

            //sort using product comparer
           // products.Sort(new ProductComparer());

            //sort using delegate
            products.Sort((x,y)=> string.Compare(x.Name, y.Name, StringComparison.Ordinal));

            //sort using LINQ
          var productsr =  products.OrderBy(p => p.Name);

            foreach (var product in productsr)
            {
                Console.WriteLine(product.Name);
            }

            Console.Read();
        } 
    }
}
