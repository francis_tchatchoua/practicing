﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        private static List<List<int>> GetLargestHourGlass(int[][] arr)
        {
            var hourGlasses = new List<List<int>>();

            if (arr.Length < 1) return default(List<List<int>>);

            foreach (var item in arr.Select((value, index) => new { value, index }))
            {
                var row1 = item.index < arr.Length ? arr[item.index] : null;
                var row2 = item.index+1 < arr.Length ? arr[item.index+1] : null;
                var row3 = item.index+2 < arr.Length ? arr[item.index+2] : null;

                if (row1 == null || row2 == null || row3 == null)
                continue;

                for (var ri = 0; ri <= row1.Length; ri++)
                {
                    var portion1 = row1.Where((x, i) => i >= ri && i < ri + 3).ToArray();
                    var portion2 = row2.Where((x, i) => i >= ri && i < ri + 3).ToArray();
                    var portion3 = row3.Where((x, i) => i >= ri && i < ri + 3).ToArray();

                    if (portion1.Length < 3 || portion2.Length < 3 || portion3.Length < 3) continue;

                   
                    var glass = portion1.Concat(portion3).Concat(portion2.Where((x,i)=> i==1)).ToList();

                    hourGlasses.Add(glass);
                }
                

            }

            return hourGlasses;

        }

        private static int GetLargestSum(List<List<int>> hourGlasses)
        {
            var sums = new List<int>();
            foreach (var glass in hourGlasses)
            {
                sums.Add(glass.Sum());
            }

            return sums.Max();
        }
        static void Main(string[] args)
        {
            var input = new[]
            {
                new[] {1, 1, 1, 0, 0, 0},
                new[] {0, 1, 0, 0, 0, 0},
                new[] {1, 1, 1, 0, 0, 0},
                new[] {0, 0, 2, 4, 4, 0},
                new[] {0, 0, 0, 2, 0, 0},
                new[] {0, 0, 1, 2, 4, 0}

            };


            var results = GetLargestHourGlass(input);

            var largesSum = GetLargestSum(results);
            Console.Read();
        }
    }
}
