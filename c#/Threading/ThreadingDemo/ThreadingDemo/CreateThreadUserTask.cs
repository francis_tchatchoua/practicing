﻿using System;
using System.Threading.Tasks;

namespace ThreadingDemo
{
    public class CreateThreadUserTask
    {
        public static void Start()
        {
            var task = new Task<int>(Calculate,100);
             task.Start();
            task.Wait();

            var result = task.Result;

            Console.WriteLine("results of addition :{0}",result);
        }

        private static int Calculate(object n)
        {
            var limit = (int) n;
            var result = 0;
            for (int i = 0; i < limit; i++)
            {
                result += i;

            }

            return result;
        }
    }
}
