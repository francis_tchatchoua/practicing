﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace ThreadingDemo
{
    public class CreateThreadPoolThreadBasic
    {
        public static void Start()
        {
            Console.WriteLine("Inside main Thread");
            CallContext.LogicalSetData("name", "patou");

            ExecutionContext.SuppressFlow();

            ThreadPool.QueueUserWorkItem(DoSomeWork, 5);
            Thread.Sleep(1000);
            Console.WriteLine("after do somework is called");



            var name = CallContext.LogicalGetData("name");


            Console.WriteLine("what is the name:{0} ", name);
        }

        private static void DoSomeWork(object state)
        {
            var name = CallContext.LogicalGetData("name");
            Console.WriteLine("inside do some work method {0}, name :{1}", state, name);
        }
    }
}
