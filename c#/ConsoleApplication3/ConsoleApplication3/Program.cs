﻿using System;
using System.Collections.Generic;

public class Movie
{
    public DateTime Start { get; private set; }
    public DateTime End { get; private set; }

    public Movie(DateTime start, DateTime end)
    {
        Start = start;
        End = end;
    }
}

public static class MovieNight
{
    public static bool CanViewAll(IEnumerable<Movie> movies)
    {
        DateTime start = new DateTime();
        DateTime end = new DateTime();
        foreach (var movie in movies)
        {
            if (start != DateTime.MinValue && end != DateTime.MinValue)
            {
                if (start < movie.End && end > movie.Start)
                {
                    return false;
                }
            }
            start = movie.Start;
            end = movie.End;
        }
        return true;
    }

    public static void Main(string[] args)
    {
        var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;

        Movie[] movies = new Movie[]
        {
            new Movie(DateTime.Parse("1/1/2015 20:00", format), DateTime.Parse("1/1/2015 21:30", format)),
            new Movie(DateTime.Parse("1/1/2015 21:30", format), DateTime.Parse("1/1/2015 23:00", format)),
            new Movie(DateTime.Parse("1/1/2015 23:10", format), DateTime.Parse("1/1/2015 23:30", format))
        };

        Console.WriteLine(MovieNight.CanViewAll(movies));
        Console.Read();
    }
}