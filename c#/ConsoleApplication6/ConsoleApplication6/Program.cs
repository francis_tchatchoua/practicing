﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApplication6
{
      class Derived:Base
    {
        public void Test()
        {
            base.Test();
        }
    }

    class Base
    {
        protected void Test()
        {
            Console.WriteLine("From derived");
        }

        internal int Me {  get;  set; }
    }
    class Program
    {
        
        static void Main(string[] args)
        {
           StreamWriter test =new StreamWriter("c:/test_patout.txt");
            test.WriteLine("test");
          
            test.Close();
            test.Dispose();
            Console.Read();
        }

        public static void Hello(out int hey)
        {
            hey = 3;
        }
    }
}
