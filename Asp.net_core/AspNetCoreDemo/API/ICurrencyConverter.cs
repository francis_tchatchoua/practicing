﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API
{
    public interface ICurrencyConverter<TSource>
    {
        ICurrencyConfiguration<TSource> Configure();
    }

    public interface ICurrencyConfiguration<T>
    {
        ICurrencyConfiguration<T> Prop(Expression<Func<T, decimal>> action);
        ICurrencyConfiguration<T> ForEach(Expression<Func<T, List<decimal>>> action);
        ICurrencyConfiguration<T> ForEach<T2>(Func<T, List<T2>> action, Action<ICurrencyConfiguration<T2>> configuration);
        void Apply(T source, decimal factor);
    }
}
