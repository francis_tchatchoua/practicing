﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class PersonController : Controller
    {
       
        [HttpGet]
        public IActionResult Get()
        {
            var converter = new CurrencyConverter<Model>().Configure();

            var configuration = converter.Prop(s => s.Premuim)
                .Prop(s => s.Debt)
                .Prop(s => s.FinanacingOptions.Cash)
                .ForEach(s => s.MetricValues)
                .ForEach(s => s.Peers, c => c.Prop(p => p.Premuin).Prop(p => p.Loss));

            var model = new Model() {Debt = 1, Premuim = 2,
                FinanacingOptions = new FinanacingOptions() {Cash = 3},
                MetricValues = new List<decimal>() { 1,2,3,4,5,7,8,9},
                Peers = new List<Peer>()
                {
                    new Peer(){ Loss = 1,Premuin = 1},
                    new Peer(){ Loss = 3,Premuin = 4}
                }
            };


            configuration.Apply(model,10000000);

           return Ok(model);

        }
    }
}
