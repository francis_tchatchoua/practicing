﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API
{
    public class Model
    {
        public decimal Premuim { get; set; }
        public decimal Debt { get; set; }
        public FinanacingOptions FinanacingOptions { get; set; }=new FinanacingOptions();
        public List<decimal> MetricValues { get; set; }

        public List<Peer> Peers { get; set; }


    }

    public class Peer
    {
        public decimal Premuin { get; set; }
        public decimal Loss  { get; set; }

        public int CompanyCount { get; set; }
    }
    public class FinanacingOptions
    {
        public decimal Cash { get; set; }

    }
}
