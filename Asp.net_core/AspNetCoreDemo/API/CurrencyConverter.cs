﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace API
{
    public class CurrencyConverter<TScource>: ICurrencyConverter<TScource>
    {
        
        public class CurrencyConfiguration<T>: ICurrencyConfiguration<T>
        {
            List<IConfigurationItem<T>> _items = new List<IConfigurationItem<T>>();

            public ICurrencyConfiguration<T> Prop(Expression<Func<T, decimal>> action)
            {
                var propertyItem = new PropertyItem<T> { Getter = action.Compile() };

                MemberExpression member = (MemberExpression)action.Body;

                ParameterExpression parameter = Expression.Parameter(typeof(decimal), "value");

                Expression<Action<T, decimal>> assign = Expression.Lambda<Action<T, decimal>>(Expression.Assign(member, parameter),
                    action.Parameters[0], parameter);

                propertyItem.Setter = assign.Compile();

                _items.Add(propertyItem);

                return this;
            }

            public ICurrencyConfiguration<T> ForEach(Expression<Func<T, List<decimal>>> action)
            {
                var propertyItem = new ListPropertyItem<T> { Getter = action.Compile() };

                MemberExpression member = (MemberExpression)action.Body;

                ParameterExpression parameter = Expression.Parameter(typeof(List<decimal>), "value");

                Expression<Action<T, List<decimal>>> assign = Expression.Lambda<Action<T, List<decimal>>>(Expression.Assign(member, parameter),
                    action.Parameters[0], parameter);

                propertyItem.Setter = assign.Compile();

                _items.Add(propertyItem);

                return this;
            }
            public ICurrencyConfiguration<T> ForEach<T2>(Func<T, List<T2>> action,Action<ICurrencyConfiguration<T2>> configuration)
            {
                var subconfig = new CurrencyConfiguration<T2>();
                configuration(subconfig);

                var item = new SubconfigItem<T, T2>
                {
                    Config = subconfig,
                    Accessor = action
                };
                _items.Add(item);
                return this;
            }

            public void Apply(T source, decimal factor)
            {
                foreach (var item in _items)
                {
                    item.Apply(source,factor);
                }
            }

            public class SubconfigItem<T, T2> : IConfigurationItem<T>
            {
                public ICurrencyConfiguration<T2> Config { get; set; }
                public Func<T, List<T2>> Accessor { get; set; }
                public void Apply(T root, decimal factor)
                {
                    try
                    {
                        var val = Accessor(root);
                        if (val == null)
                            return;
                        foreach (var item in val)
                        {
                            Config.Apply(item, factor);
                        }
                    }
                    catch (NullReferenceException) { }
                }
            }
        }
        public ICurrencyConfiguration<TScource> Configure()
        {
          var config  = new CurrencyConfiguration<TScource>();

          return config;
        }

        public class PropertyItem<T>:IConfigurationItem<T>
        {
            public Func<T, decimal> Getter { get; set; }
            public  Action<T,decimal> Setter { get; set; }

            public void Apply(T source,decimal factor)
            {
                var value = Getter(source);

                Setter(source, value * factor);
            }
        }

        public class ListPropertyItem<T> : IConfigurationItem<T>
        {
            public Func<T, List<decimal>> Getter { get; set; }
            public Action<T, List<decimal>> Setter { get; set; }

            public void Apply(T source, decimal factor)
            {
                var results = new List<decimal>();
                var values = Getter(source);

                foreach (var value in values)
                {
                    results.Add(value*factor);
                }

                Setter(source, results);
            }
        }
    }
}
