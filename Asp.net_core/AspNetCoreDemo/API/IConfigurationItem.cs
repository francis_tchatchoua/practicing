﻿namespace API
{
    public interface IConfigurationItem<T>
    {
        void Apply(T source,decimal factor);
    }
}
