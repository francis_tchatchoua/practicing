<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'francisBlog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'Pescador@90');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'e<7d1(<)]s.ZQiT.:|}NTcD|0V2|$O+H`RtA=,Bj#KQ3/$$R[?2%;WuY[W `s+9K');
define('SECURE_AUTH_KEY',  'zrI%s*2]T:|@H1a3k3X<wfXXE?M]HlJd?NYvrQDwJ9JBS{ha_^H/FK&.5-|r3-6^');
define('LOGGED_IN_KEY',    '8F@@[`+}Hov:Qg72*sX,Jm^nllnB|A[)O9k6WOP0d/<ant}UTiAKm+`Y!O*k$C`*');
define('NONCE_KEY',        ';eutQD,G@;il,vl|A)N[/Zi+xk[!*8cmoJDn5&LX~w|$xGZss.LUjf2nJ#TVWhqC');
define('AUTH_SALT',        '7$QGD(/VVbA-[l;D$dzZ3fVeux5/R%xjty.q{ztT<B.r=C{^t.EU^IsqnBVwK-<#');
define('SECURE_AUTH_SALT', ':`XwEwokD,h97-QX4;C/g%,Fg[f-*v|kK%!{=Pk0aMK>HyH#bhI(q=GIoq::|AaW');
define('LOGGED_IN_SALT',   'zR]uu@Rm5ID.V#}PI$a22X}ZZFRGp:QyvwSM$fZMa|9FRh^K}P7~pOk22$-8JE6v');
define('NONCE_SALT',       '#U>~jo6(pUcYtqLOyEt<qcE!gw<.KED:vxjhU>L=/ywd%L7G2wvm@EzsTw!}zjKw');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
