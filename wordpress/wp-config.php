<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'francisBlog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'Pescador@90');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aK<*,|}|]0v[1Bx2#NrJNI)U%-j+0L,U-koJ/nYUeN}h4$bt.3m$}`WSuB[F&Z+p');
define('SECURE_AUTH_KEY',  'auA$V;+TDuxU&N1@`eCt#+&<LSGqn++G,J&M8HwszO_~=MbarW6o5}pT9i(6p)Z@');
define('LOGGED_IN_KEY',    'lBthSE<VO*K1$ip3R1BdfnC^~I)<dg>.$+B7f+ %S?wht}w..]%|RPEl<=+9C+<z');
define('NONCE_KEY',        '-!eKo39^.9wNsuaoDJfLoH*K,0;---R1(Q@ha&HJM8|Xl9YvA-K{@rL2spH)`Z:<');
define('AUTH_SALT',        'i*PEqIE-G+G4Y?)JbJsx+-[Mn;psR@!]@X4]LvNtsdFMeNX-.CjnmtcSMMFn0/TT');
define('SECURE_AUTH_SALT', 'Cpqk4uv^:Q.Ds+zaDcA? AKVaQ|7Pd9isbn${=tCD!|sW<:mYPS{XvtWz)I?`fn7');
define('LOGGED_IN_SALT',   'dAopz_S#ku1-d81|SY-FRIO||OLv5_--W&u=LW:Sv}h W+IXHw.Qi[j;LjV}@ ]z');
define('NONCE_SALT',       'KP9}cYC3%Ja.mORvWM9Qe]Lw(}Uh]&l@?JV@0P ~o,Wu~*Z~An#S`#q+5~9B*a/[');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
