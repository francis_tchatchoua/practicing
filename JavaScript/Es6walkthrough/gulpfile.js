var gulp = require('gulp');
var traceur = require('gulp-traceur');
var babel = require('gulp-babel');

gulp.task('traceur',function(){
	return gulp.src('src_es6/**/*.js')
				.pipe(traceur({experimental: true, blockBinding: true}))
				.pipe(gulp.dest('dest_es5'));
});

gulp.task('b6to5', function(){
	return gulp.src('src_es6/**/*.js')
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(gulp.dest('dest_es5'));
});

gulp.task('6to5',['b6to5']);