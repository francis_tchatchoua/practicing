let values = [1, 2, 3];

let iterator = values[Symbol.iterator]();
console.log(iterator.next());
console.log('-------------------------------looping below-----------------------------------');

for (let num of values) {
    console.log(num);
}

console.log('-----------------------check if object is iterable--------------------------------');

function isIterable(object) {
    return typeof object[Symbol.iterator] === "function";
}

console.log(isIterable([1, 2, 3]));     // true
console.log(isIterable("Hello"));       // true
console.log(isIterable(new Map()));     // true
console.log(isIterable(new Set()));     // true
console.log(isIterable(new WeakMap())); // false
console.log(isIterable(new WeakSet())); // false

