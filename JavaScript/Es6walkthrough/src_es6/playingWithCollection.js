let colors = [ "red", "green", "blue" ];
let tracking = new Set([1234, 5678, 9012]);

let data = new Map();
data.set("title", "Understanding ECMAScript 6");
data.set("format", "ebook");

console.log('------------------- checking collection entry() function -------------------------------')

for (let entry of colors.entries()) {
    console.log(entry);
}
for (let entry of tracking.entries()) {
    console.log(entry);
}
for (let entry of data.entries()) {
    console.log(entry);
}

console.log('------------------- checking collection values() function -------------------------------')

for (let value of colors.values()) {
    console.log(value);
}
for (let value of tracking.values()) {
    console.log(value);
}
for (let value of data.values()) {
    console.log(value);
}


console.log('------------------- checking collection keys() function -------------------------------')

for (let key of colors.keys()) {
    console.log(key);
}
for (let key of tracking.keys()) {
    console.log(key);
}
for (let key of data.keys()) {
    console.log(key);
}
console.log('------------------- playing with string collection -------------------------------')
var message = "A ð ®· B";

for (let c of message) {
    console.log(c);
}