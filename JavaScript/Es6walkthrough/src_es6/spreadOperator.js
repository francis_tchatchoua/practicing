import "babel-polyfill";

let set = new Set([1, 2, 3, 3, 3, 4, 5]),
    array = [...set];

console.log(array);

console.log('----------------------merging arrays ------------------------------------');
let smallNumbers = [1, 2, 3],
    bigNumbers = [100, 101, 102],
    allNumbers = [0, ...smallNumbers, ...bigNumbers];

console.log(allNumbers.length);
console.log(allNumbers);