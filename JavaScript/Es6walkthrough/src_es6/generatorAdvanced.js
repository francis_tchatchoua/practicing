import "babel-polyfill";

function *createIterator() {
    let first = yield 1;
    console.log(first);
    let second = yield first + 2;
    console.log(second);       
    let three =yield second + 3;
    console.log(three);                 
}

let iterator = createIterator();

console.log(iterator.next(2));
console.log(iterator.next(4)); 
console.log(iterator.next(5)); 
console.log(iterator.next());  