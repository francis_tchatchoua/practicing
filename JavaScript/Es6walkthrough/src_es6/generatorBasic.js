import "babel-polyfill";

/*var o = {

    createIterator: function *(items) {
        for (let i=0; i < items.length; i++) {
            yield items[i];
        }
    }
};*/

var o = {

    *createIterator (items) {
        for (let i=0; i < items.length; i++) {
            yield items[i];
        }
    }
};

var iterator = o.createIterator([1,2,3]);


for (let next of o.createIterator([1,2,3])) {
	console.log(next);
}