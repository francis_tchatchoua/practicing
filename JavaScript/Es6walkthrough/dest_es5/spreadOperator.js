"use strict";

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

require("babel-polyfill");

var set = new Set([1, 2, 3, 3, 3, 4, 5]),
    array = [].concat(_toConsumableArray(set));

console.log(array);

console.log('----------------------merging arrays ------------------------------------');
var smallNumbers = [1, 2, 3],
    bigNumbers = [100, 101, 102],
    allNumbers = [0].concat(smallNumbers, bigNumbers);

console.log(allNumbers.length);
console.log(allNumbers);