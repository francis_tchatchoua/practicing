"use strict";
require("babel-polyfill");
var marked0$0 = [createIterator].map(regeneratorRuntime.mark);



function createIterator() {
    var first, second, three;
    return regeneratorRuntime.wrap(function createIterator$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                context$1$0.next = 2;
                return 1;

            case 2:
                first = context$1$0.sent;

                console.log(first);
                context$1$0.next = 6;
                return first + 2;

            case 6:
                second = context$1$0.sent;

                console.log(second);
                context$1$0.next = 10;
                return second + 3;

            case 10:
                three = context$1$0.sent;

                console.log(three);

            case 12:
            case "end":
                return context$1$0.stop();
        }
    }, marked0$0[0], this);
}

var iterator = createIterator();

console.log(iterator.next(2));
console.log(iterator.next(4));
console.log(iterator.next(5));
console.log(iterator.next());