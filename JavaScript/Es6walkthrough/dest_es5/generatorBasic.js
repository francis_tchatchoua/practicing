"use strict";

require("babel-polyfill");

/*var o = {

    createIterator: function *(items) {
        for (let i=0; i < items.length; i++) {
            yield items[i];
        }
    }
};*/

var o = {

    createIterator: regeneratorRuntime.mark(function createIterator(items) {
        var i;
        return regeneratorRuntime.wrap(function createIterator$(context$1$0) {
            while (1) switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    i = 0;

                case 1:
                    if (!(i < items.length)) {
                        context$1$0.next = 7;
                        break;
                    }

                    context$1$0.next = 4;
                    return items[i];

                case 4:
                    i++;
                    context$1$0.next = 1;
                    break;

                case 7:
                case "end":
                    return context$1$0.stop();
            }
        }, createIterator, this);
    })
};

var iterator = o.createIterator([1, 2, 3]);

var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
    for (var _iterator = o.createIterator([1, 2, 3])[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var next = _step.value;

        console.log(next);
    }
} catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
} finally {
    try {
        if (!_iteratorNormalCompletion && _iterator["return"]) {
            _iterator["return"]();
        }
    } finally {
        if (_didIteratorError) {
            throw _iteratorError;
        }
    }
}