import { NgModule }           from '@angular/core';
import { CommonModule }       from '@angular/common';
import { FormsModule }        from '@angular/forms';
import {HeroesComponent} from './heroes.component';
import {HeroDetailComponent} from './hero-detail.component';
import {HeroesComponent as HeroListComponent} from './heroes.component';


@NgModule({
  imports: [ CommonModule, FormsModule],
  declarations: [
    HeroesComponent, HeroDetailComponent, HeroListComponent,
  ]
})
export default class HeroModule { }
