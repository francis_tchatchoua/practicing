import { NgModule }       from '@angular/core';
import {CommonModule} from '@angular/common';
import { BrowserModule }  from '@angular/platform-browser';

import { AppComponent }   from './app.component';
import {NannyListComponent} from './nannies.component';
import {WikipediaService } from './services/wikipedia.service';
import {NannyService} from './services/nanny.service';
import { HttpModule} from '@angular/http';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import {JSONP_PROVIDERS} from '@angular/http';

import {routing} from './app.routing';

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        routing,
        HttpModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [AppComponent,
        NannyListComponent],
    providers:[NannyService,WikipediaService,JSONP_PROVIDERS],
    bootstrap: [AppComponent],
})
export class AppModule { }