"use strict";
var router_1 = require('@angular/router');
var nannies_component_1 = require('./nannies.component');
var appRoutes = [
    {
        path: 'nannies',
        component: nannies_component_1.NannyListComponent
    },
    {
        path: '',
        redirectTo: '/nannies',
        pathMatch: 'full'
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map