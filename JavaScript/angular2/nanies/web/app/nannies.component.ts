import {Component, OnInit} from '@angular/core';
import {NannyService} from './services/nanny.service';
import {Nanny} from './model/nanny';


@Component({
    styleUrls: ['app/nannies.component.css'],
    templateUrl: 'app/nannies.component.html'
})
export class NannyListComponent implements OnInit {
    nannies: Nanny[];
    errorMessage: string = '';

    constructor(private nannyService: NannyService) {
        this.nannyService.getAll().subscribe(
            nannies => this.nannies = nannies,
            error => this.errorMessage = <any>error);
    }
    ngOnInit() {

    }
}