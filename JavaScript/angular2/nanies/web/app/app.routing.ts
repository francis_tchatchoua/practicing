import {Routes,RouterModule} from '@angular/router';

import {NannyListComponent} from './nannies.component';

const appRoutes: Routes = [
    {
        path: 'nannies',
        component: NannyListComponent
    },
    {
        path: '',
        redirectTo: '/nannies',
        pathMatch: 'full'
    }
];

export const routing = RouterModule.forRoot(appRoutes);
