import {Component} from '@angular/core';
import './rxjs-operators';
import {FormControl,FormGroup,AbstractControl} from '@angular/forms'
import {TestComponent} from './test.component';
import {WikipediaService} from './services/wikipedia.service';
import {Observable} from 'rxjs';


@Component({
    selector:'nanny',
    styleUrls:['app/app.component.css'],
    templateUrl:'app/app.component.html',
    directives:[TestComponent]
})
export class AppComponent {
    items: Observable<Array<string>>;
    term = new FormControl();
    constructor(private wikipediaService: WikipediaService) {
      this.items=  this.term.valueChanges
                .debounceTime(400)
                .distinctUntilChanged()
                .switchMap(term => this.wikipediaService.search(term))
              //.flatMap(term => this.wikipediaService.search(term))
              //.subscribe(items => this.items = items);
    }
}