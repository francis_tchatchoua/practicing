import {Injectable} from '@angular/core';
import {Nanny} from '../model/nanny';
import { Observable }     from 'rxjs/Observable';
import { Http, Response } from '@angular/http';

const API_URL='http://localhost:3001/nannies'

@Injectable()
export class NannyService {
    constructor(private http: Http) {}
    
    getAll(): Observable<Nanny[]> {
        return this.http.get(API_URL)
            .map(this.extractData)
            .catch(this.handleError);
    }
    
    private extractData(res: Response) {
        let nannies = res.json();
        return nannies || {};
    }
    private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
  }
}