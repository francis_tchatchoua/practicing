var express = require('express');
var http = require('http');

var app = express();

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
});

app.get('/nannies', function (req, res) {
    var nannies = [
        { id: 1, name: 'Joanna Czchowska' },
        { id: 2, name: 'Carine Noundou' },
        { id: 3, name: 'Mary Mbianga' },
        { id: 4, name: 'Charlotte Nkuidjeu' },
        { id: 5, name: 'Nama Darius' }
    ]
    res.send(nannies);
});
var server = http.createServer(app, function () {
    console.log('application listening on port 3001');
});

server.listen(3001);