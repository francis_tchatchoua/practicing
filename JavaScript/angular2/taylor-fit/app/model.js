"use strict";
var Doors = (function () {
    function Doors() {
    }
    return Doors;
}());
var Car = (function () {
    function Car() {
    }
    return Car;
}());
var Tires = (function () {
    function Tires() {
    }
    return Tires;
}());
var Engine = (function () {
    function Engine() {
    }
    return Engine;
}());
var core_1 = require('@angular/core');
var injector = core_1.ReflectiveInjector.resolveAndCreate([
    Car,
    Engine,
    Tires,
    Doors
]);
var Main = (function () {
    function Main() {
        var car = injector.get(Car);
        console.dir(car);
    }
    return Main;
}());
exports.Main = Main;
//# sourceMappingURL=model.js.map