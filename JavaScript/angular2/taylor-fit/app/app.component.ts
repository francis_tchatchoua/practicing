import {Component,forwardRef,OnInit,Inject} from '@angular/core';
import {FormBuilder,FormGroup} from '@angular/forms';
import {createCounterRangeValidator} from './count-input.component'
import {Car,Engine,Doors,Tires,OtherEngine} from './inject-demo.injector';
 import { ReflectiveInjector } from '@angular/core';

var injector = ReflectiveInjector.resolveAndCreate([
                {provide:Engine, useClass:OtherEngine},
                {provide:Tires, useValue:'using the tires'},
                {provide:Doors, useFactory:() =>{ return new Doors()}}
                ]);
                
let childInjector =injector.resolveAndCreateChild([Car])
@Component({
    moduleId:module.id,
    selector:'taylor-fit',
    templateUrl:'app.component.html'
})
export class AppComponent implements OnInit {
  outerCounterValue:any=5;
  form: FormGroup;

  constructor(private fb: FormBuilder,@Inject(forwardRef(()=>NameService)) nameService) {
      
        let car = childInjector.get(Car);
        
        car.log();  
        console.log(nameService.getName());      
      
  }

  ngOnInit() {
    this.form = this.fb.group({
      counter: [5,createCounterRangeValidator]
    });
  }
}

export class NameService {
  name = 'Pascal';;

  getName() {
    return this.name;
  }
}