"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
function createCounterRangeValidator(maxValue, minValue) {
    return function validateCounterRange(c) {
        var err = {
            rangeError: {
                given: c.value,
                max: maxValue,
                min: minValue
            }
        };
        return (c.value > +maxValue || c.value < +minValue) ? err : null;
    };
}
exports.createCounterRangeValidator = createCounterRangeValidator;
var CounterInputComponent = (function () {
    function CounterInputComponent() {
        this.propagateChange = function (_) { };
        this._counterValue = 0; // notice the '_
    }
    Object.defineProperty(CounterInputComponent.prototype, "counterValue", {
        get: function () {
            return this._counterValue;
        },
        set: function (val) {
            this._counterValue = val;
            this.propagateChange(this._counterValue);
        },
        enumerable: true,
        configurable: true
    });
    CounterInputComponent.prototype.increment = function () {
        this.counterValue++;
    };
    CounterInputComponent.prototype.decrement = function () {
        this.counterValue--;
    };
    CounterInputComponent.prototype.writeValue = function (value) {
        if (value !== undefined) {
            this.counterValue = value;
        }
    };
    CounterInputComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    CounterInputComponent.prototype.registerOnTouched = function () { };
    CounterInputComponent.prototype.ngOnInit = function () {
        this.validateFn = createCounterRangeValidator(this.counterRangeMax, this.counterRangeMin);
    };
    CounterInputComponent.prototype.validate = function (c) {
        return this.validateFn(c);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], CounterInputComponent.prototype, "_counterValue", void 0);
    __decorate([
        // notice the '_
        core_1.Input(), 
        __metadata('design:type', Object)
    ], CounterInputComponent.prototype, "counterRangeMax", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], CounterInputComponent.prototype, "counterRangeMin", void 0);
    CounterInputComponent = __decorate([
        core_1.Component({
            selector: 'counter-input',
            template: "\n    <button (click)=\"increment()\">+</button>\n    {{counterValue}}\n    <button (click)=\"decrement()\">-</button>\n  ",
            providers: [
                {
                    provide: forms_1.NG_VALUE_ACCESSOR,
                    useExisting: core_1.forwardRef(function () { return CounterInputComponent; }),
                    multi: true
                },
                { provide: forms_1.NG_VALIDATORS, useExisting: core_1.forwardRef(function () { return CounterInputComponent; }), multi: true }
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], CounterInputComponent);
    return CounterInputComponent;
}());
exports.CounterInputComponent = CounterInputComponent;
//# sourceMappingURL=count-input.component.js.map