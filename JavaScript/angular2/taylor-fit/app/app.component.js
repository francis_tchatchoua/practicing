"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var count_input_component_1 = require('./count-input.component');
var inject_demo_injector_1 = require('./inject-demo.injector');
var core_2 = require('@angular/core');
var injector = core_2.ReflectiveInjector.resolveAndCreate([
    { provide: inject_demo_injector_1.Engine, useClass: inject_demo_injector_1.OtherEngine },
    { provide: inject_demo_injector_1.Tires, useValue: 'using the tires' },
    { provide: inject_demo_injector_1.Doors, useFactory: function () { return new inject_demo_injector_1.Doors(); } }
]);
var childInjector = injector.resolveAndCreateChild([inject_demo_injector_1.Car]);
var AppComponent = (function () {
    function AppComponent(fb, nameService) {
        this.fb = fb;
        this.outerCounterValue = 5;
        var car = childInjector.get(inject_demo_injector_1.Car);
        car.log();
        console.log(nameService.getName());
    }
    AppComponent.prototype.ngOnInit = function () {
        this.form = this.fb.group({
            counter: [5, count_input_component_1.createCounterRangeValidator]
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'taylor-fit',
            templateUrl: 'app.component.html'
        }),
        __param(1, core_1.Inject(core_1.forwardRef(function () { return NameService; }))), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, Object])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
var NameService = (function () {
    function NameService() {
        this.name = 'Pascal';
    }
    ;
    NameService.prototype.getName = function () {
        return this.name;
    };
    return NameService;
}());
exports.NameService = NameService;
//# sourceMappingURL=app.component.js.map