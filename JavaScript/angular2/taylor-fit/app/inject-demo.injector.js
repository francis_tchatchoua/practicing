"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Doors = (function () {
    function Doors() {
    }
    return Doors;
}());
exports.Doors = Doors;
var Tires = (function () {
    function Tires() {
    }
    return Tires;
}());
exports.Tires = Tires;
var Engine = (function () {
    function Engine() {
    }
    return Engine;
}());
exports.Engine = Engine;
var OtherEngine = (function () {
    function OtherEngine() {
    }
    return OtherEngine;
}());
exports.OtherEngine = OtherEngine;
var Car = (function () {
    // constructor(@Inject(Engine) engine,@Inject(Tires) tires,@Inject(Doors) doors){
    //      this._engine=engine;
    //      this._tires=tires;
    //      this._doors=doors;
    // }
    function Car(engine, tires, doors) {
        this._engine = engine;
        this._tires = tires;
        this._doors = doors;
    }
    Car.prototype.log = function () {
        console.dir(this._engine);
        console.dir(this._tires);
        console.dir(this._doors);
    };
    Car = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [Engine, Tires, Doors])
    ], Car);
    return Car;
}());
exports.Car = Car;
//# sourceMappingURL=inject-demo.injector.js.map