 import {Inject,Injectable} from '@angular/core';

 
 export class Doors {
    
}
 export class Tires {
    
}

export class Engine {
    
}
export class OtherEngine {
    
}


@Injectable()
export class Car {
    _engine:Engine;
    _tires:Tires;
    _doors:Doors;
    
    // constructor(@Inject(Engine) engine,@Inject(Tires) tires,@Inject(Doors) doors){
    //      this._engine=engine;
    //      this._tires=tires;
    //      this._doors=doors;
    // }
    constructor(engine:Engine,tires:Tires,doors:Doors){
         this._engine=engine;
         this._tires=tires;
         this._doors=doors;
    }
    log(){
        console.dir(this._engine);
        console.dir(this._tires);
        console.dir(this._doors);
    }
}
