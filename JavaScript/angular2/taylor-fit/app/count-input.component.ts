import { Component, Input,forwardRef } from '@angular/core';
import {ControlValueAccessor,NG_VALUE_ACCESSOR,NG_VALIDATORS,FormControl} from '@angular/forms';

export function createCounterRangeValidator(maxValue, minValue) {
  return function validateCounterRange(c: FormControl) {
    let err = {
      rangeError: {
        given: c.value,
        max: maxValue,
        min: minValue
      }
    };

    return (c.value > +maxValue || c.value < +minValue) ? err: null;
  }
}

@Component({
  selector: 'counter-input',
  template: `
    <button (click)="increment()">+</button>
    {{counterValue}}
    <button (click)="decrement()">-</button>
  `,
   providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CounterInputComponent),
      multi: true
    },
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => CounterInputComponent), multi: true }
  ]
})
export class CounterInputComponent  implements ControlValueAccessor {

  propagateChange = (_: any) => {};
  validateFn:Function;
  @Input()
  _counterValue = 0; // notice the '_
  
  @Input()
  counterRangeMax;

  @Input()
  counterRangeMin;
  
  get counterValue() {
    return this._counterValue;
  }

  set counterValue(val) {
    this._counterValue = val;
    this.propagateChange(this._counterValue);
  }

  increment() {
    this.counterValue++;
  }

  decrement() {
    this.counterValue--;
  }
  
  writeValue(value: any) {
        if (value !== undefined) {
            this.counterValue = value;
        }
   
}
registerOnChange(fn) {
    this.propagateChange = fn;
}

registerOnTouched() {}

ngOnInit() {
    this.validateFn = createCounterRangeValidator(this.counterRangeMax, this.counterRangeMin);
}

  validate(c: FormControl) {
    return this.validateFn(c);
  }
}


