"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
exports.CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
    provide: forms_1.NG_VALUE_ACCESSOR,
    useExisting: core_1.forwardRef(function () { return PercentFormat; }),
    multi: true
};
var PercentFormat = (function () {
    function PercentFormat(el, renderer) {
        // renderer.setElementStyle(el.nativeElement, 'backgroundColor', 'yellow');       
    }
    PercentFormat.prototype.onChange = function (e) {
    };
    PercentFormat.prototype.writeValue = function (value) {
    };
    PercentFormat.prototype.registerOnChange = function (fn) {
        debugger;
        this.onChangeCallback = fn;
    };
    PercentFormat.prototype.registerOnTouched = function (fn) {
        debugger;
        this.onTouchedCallback = fn;
    };
    PercentFormat = __decorate([
        core_1.Directive({
            selector: 'input[percent-format]',
            host: {
                '(mouseenter)': 'onChange($event)'
            },
            providers: [exports.CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer])
    ], PercentFormat);
    return PercentFormat;
}());
exports.PercentFormat = PercentFormat;
//# sourceMappingURL=percent-format.directive.js.map