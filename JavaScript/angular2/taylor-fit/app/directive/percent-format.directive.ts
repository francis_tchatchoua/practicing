import {Directive,ElementRef,Renderer,HostListener,forwardRef } from '@angular/core';
import {ControlValueAccessor,NG_VALUE_ACCESSOR} from '@angular/forms';

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => PercentFormat),
    multi: true
};
@Directive(
    {
        selector:'input[percent-format]',
        host:{
            '(mouseenter)':'onChange($event)'
        },
        providers:[CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
    }
)
export class PercentFormat  implements ControlValueAccessor{
    constructor(el: ElementRef, renderer: Renderer) {
      // renderer.setElementStyle(el.nativeElement, 'backgroundColor', 'yellow');       
    }
    
    private onTouchedCallback: () => void;
    private onChangeCallback: (_: any) => void;

    onChange(e):void{
    }
    
    writeValue(value:any){
        
    }
    registerOnChange(fn: any) {
        debugger;
        this.onChangeCallback = fn;
    }
    registerOnTouched(fn: any) {
         debugger;
        this.onTouchedCallback = fn;
    }

}
