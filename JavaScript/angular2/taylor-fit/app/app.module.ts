import { NgModule }       from '@angular/core';
import {CommonModule} from '@angular/common';
import { BrowserModule }  from '@angular/platform-browser';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import {PercentFormat} from './directive/percent-format.directive';

import { AppComponent,NameService }   from './app.component';
import {CounterInputComponent} from './count-input.component';


@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers:[ 
        {provide:NameService, useClass:NameService}
    ],
    declarations: [AppComponent,PercentFormat,CounterInputComponent],
    bootstrap: [AppComponent],
})
export class AppModule { }