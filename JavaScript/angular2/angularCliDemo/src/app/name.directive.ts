import { Directive,Inject,forwardRef,OpaqueToken } from '@angular/core';
import { NG_VALIDATORS,Validator,AbstractControl,ValidatorFn  } from '@angular/forms';

const CUSTOM_VALIDATOR={
  provide:NG_VALIDATORS,
  useExisting:forwardRef(()=>NameDirective),
  multi:true
}

@Directive({
  selector: '[test-name]',
  providers:[CUSTOM_VALIDATOR]
})
export class NameDirective implements Validator {
  validate(c:AbstractControl):{[key:string]:any}{
    return {'error':true};
  }

}
