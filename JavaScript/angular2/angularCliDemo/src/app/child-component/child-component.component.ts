import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/vex';

@Component({
  selector: 'child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.css']
})
export class ChildComponentComponent {

  constructor() {}
    
}
