import { Component } from '@angular/core';

@Component({
  selector: 'form-app',
  template: `<h1>{{title}}</h1>`
})
export class AppComponent {
  title:string='form demo'
}
