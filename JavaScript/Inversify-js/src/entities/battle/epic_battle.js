"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var inversify_1 = require("inversify");
var identifiers_1 = require("../../constants/identifiers");
var tags_1 = require("../../constants/tags");
var EpicBattle = (function () {
    function EpicBattle() {
    }
    EpicBattle.prototype.fight = function () {
        var desc = "FIGHT!\n                " + this.warrior1.name + " (" + this.warrior1.weapon.name + ")\n                vs\n                " + this.warrior2.name + " (" + this.warrior2.weapon.name + ")";
        return desc;
    };
    return EpicBattle;
}());
__decorate([
    inversify_1.inject(identifiers_1.default.WARRIOR), inversify_1.named(tags_1.default.CHINESE),
    __metadata("design:type", Object)
], EpicBattle.prototype, "warrior1", void 0);
__decorate([
    inversify_1.inject(identifiers_1.default.WARRIOR), inversify_1.named(tags_1.default.JAPANESE),
    __metadata("design:type", Object)
], EpicBattle.prototype, "warrior2", void 0);
EpicBattle = __decorate([
    inversify_1.injectable()
], EpicBattle);
exports.default = EpicBattle;
