"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ioc_config_1 = require("./config/ioc_config");
var identifiers_1 = require("./constants/identifiers");
// Composition root
var epicBattle = ioc_config_1.default.get(identifiers_1.default.BATTLE);
console.log(epicBattle.fight());
