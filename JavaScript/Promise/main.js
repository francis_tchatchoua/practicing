function getMessage(n){
	
	var wait =$.Deferred();
	
	setTimeout(function(){
		wait.resolve(n);
	},n);

	return wait.promise();
};

var pro =getMessage(2000);

pro.then(function(result){
	console.log('cool pat'+result);
    return getMessage(3000)
}).then(function(data){
	console.log('cool pat'+data);
}).then(function(){

	console.log('all good');
});