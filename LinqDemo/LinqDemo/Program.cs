﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var added = new List<string>() {"francis", "kevin", "carine"};
            var removed= new List<string>()
            {
                "kevin","carine"
            };

            var results = added.Except(removed);
        }
    }
}
