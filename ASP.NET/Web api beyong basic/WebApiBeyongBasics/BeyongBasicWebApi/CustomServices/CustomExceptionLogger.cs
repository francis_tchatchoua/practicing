﻿using System.Web.Http.ExceptionHandling;
using NLog;

namespace BeyongBasicWebApi.CustomServices
{
    public class CustomExceptionLogger :ExceptionLogger
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public override void Log(ExceptionLoggerContext context)
        {
            _logger.ErrorException(context.Exception.Message, context.Exception); 
        }
    }
}