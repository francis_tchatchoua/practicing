﻿using System;
using System.Net;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace BeyongBasicWebApi.CustomServices
{
    public class CacheFilter:ActionFilterAttribute
    {
        private readonly int _maxAge;

        public CacheFilter(int maxAge)
        {
            _maxAge = maxAge;
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var statusCode = actionExecutedContext.Response.StatusCode;

            if (statusCode == HttpStatusCode.OK || statusCode == HttpStatusCode.NotModified)
            {
                var response = actionExecutedContext.Response;

                var cacheControle = new CacheControlHeaderValue
                {
                    MaxAge = TimeSpan.FromSeconds(_maxAge)
                };
                response.Headers.CacheControl = cacheControle;
            }
     
            base.OnActionExecuted(actionExecutedContext);
        }
    }
}