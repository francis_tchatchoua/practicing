﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using BeyongBasicWebApi.Model;

namespace BeyongBasicWebApi.CustomServices
{
    public class PersonWithETagResponse :IHttpActionResult
    {
        private readonly HttpRequestMessage _httpRequestMessage;
        private readonly Person _person;

        public PersonWithETagResponse(Person person,HttpRequestMessage httpRequestMessage)
        {
            _person = person;
            _httpRequestMessage = httpRequestMessage;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage response;
            if (_person == null)
            {
                response = _httpRequestMessage.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                if (GetEtageVersion() == _person.Version.ToString())
                {
                    response = _httpRequestMessage.CreateResponse(HttpStatusCode.NotModified);
                }
                else
                {
                    response = _httpRequestMessage.CreateResponse(HttpStatusCode.OK, _person);
                    _person.Version += 1;
                    SetEtage(response,_person.Version+1);
                }
            }

            return Task.FromResult(response);
        }

        private void SetEtage(HttpResponseMessage response,int value)
        {
            response.Headers.ETag = new EntityTagHeaderValue(String.Concat("\"", value.ToString(), "\"")); 
        }

        private string GetEtageVersion()
        {
            var etag = _httpRequestMessage.Headers.IfNoneMatch.FirstOrDefault();

            string etagValue;

            if (etag!= null)
            {
                etagValue = etag.ToString().Replace(@"""", "");
            }
            else
            {
                etagValue = null;
            }

            return etagValue;
        }
    }
}