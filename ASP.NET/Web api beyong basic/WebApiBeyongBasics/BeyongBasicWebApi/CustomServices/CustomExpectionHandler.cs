﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace BeyongBasicWebApi.CustomServices
{
    public class CustomExpectionHandler : ExceptionHandler
    {

        public override void Handle(ExceptionHandlerContext context)
        {
            context.Result = new InternalServerErrorResult(context.Exception, context.Request);
        }
    }


    public class InternalServerErrorResult:IHttpActionResult
    {
        private readonly Exception _exception;
        private readonly HttpRequestMessage _requestMessage;

        public InternalServerErrorResult(Exception exception,HttpRequestMessage requestMessage)
        {
            _requestMessage = requestMessage;
            _exception = exception;
        }

        public Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            var message = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                RequestMessage = _requestMessage,
                Content = new StringContent("An error occured oooop!!!!!"),
                ReasonPhrase = "oh boy!"
            };

            return message;
        }
    }
}