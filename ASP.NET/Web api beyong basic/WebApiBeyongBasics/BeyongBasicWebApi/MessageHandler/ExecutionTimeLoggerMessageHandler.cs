﻿using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace BeyongBasicWebApi.MessageHandler
{
    public class ExecutionTimeLoggerMessageHandler  : DelegatingHandler
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var watch = new Stopwatch();
            watch.Start();
            var response = await base.SendAsync(request, cancellationToken);

            watch.Stop();

            response.Headers.Add("X-ExecutionTime",watch.ElapsedMilliseconds.ToString());
            response.Headers.Add("X-Developer", "Francis Tchatchoua");
            
            //can also do some logging here
            _logger.Info("Request Execution time:{0}",watch.ElapsedMilliseconds.ToString());
            return response;
        }
    }
}