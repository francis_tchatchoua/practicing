﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Batch;
using BeyongBasicWebApi.CustomServices;
using BeyongBasicWebApi.Model;
using Newtonsoft.Json;
using WebGrease.Css.Ast.Selectors;

namespace BeyongBasicWebApi.Controllers
{
    public class PersonController : ApiController
    {
        private static List<Person> _people = new List<Person>();

        [CacheFilter(600)]
        public HttpResponseMessage Get(string name)
        {

            string root = HttpContext.Current.Server.MapPath("~/App_Data");

            var path = Path.Combine(root, "test.pdf");

            var filestream = File.OpenRead(path);

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content=new StreamContent(filestream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "test.pdf"
            };
            return response;


        }
        [CustomValidationFilter]
        public async Task<IHttpActionResult> Post( )
        {
          string root = HttpContext.Current.Server.MapPath("~/App_Data");
          var provider = new MultipartFormDataStreamProvider(root);
          await Request.Content.ReadAsMultipartAsync(provider);
          var formData = provider.FormData[0];

          var serializer = new JsonSerializer();

          var obj= JsonConvert.DeserializeObject<Person>(formData);
          var fileData = provider.FileData[0];
         
          return null;
        }
    }
}
