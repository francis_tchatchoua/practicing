﻿using System.ComponentModel.DataAnnotations;

namespace BeyongBasicWebApi.Model
{
    public class Person
    {
        [Required]
        public string FirstName { get; set; }
        public int Version { get; set; }
    }
}