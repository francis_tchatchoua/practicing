﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using BeyongBasicWebApi.CustomServices;
using BeyongBasicWebApi.MessageHandler;

namespace BeyongBasicWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //adding a new message handler
            config.MessageHandlers.Add(new ExecutionTimeLoggerMessageHandler());
            config.Services.Add(typeof(IExceptionLogger),new CustomExceptionLogger());
            config.Services.Replace(typeof(IExceptionHandler),new CustomExpectionHandler());
        }
    }
}
