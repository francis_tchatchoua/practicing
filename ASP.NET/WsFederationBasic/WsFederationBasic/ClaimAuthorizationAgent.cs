﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace WsFederationBasic
{
    public class ClaimAuthorizationAgent:ClaimsAuthorizationManager
    {
        public override bool CheckAccess(AuthorizationContext context)
        {
            var principal = context.Principal;
            var actions = context.Action.First();
            var resources = context.Resource.First();

            var profile = principal.FindFirst("http://test.com/profile").Value.Split(',');

            if (actions.Value == profile[0] && resources.Value == profile[1]) return true;
            return false;
        }
    }
}