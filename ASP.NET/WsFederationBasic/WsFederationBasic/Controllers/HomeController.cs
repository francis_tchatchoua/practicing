﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Security.Permissions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace WsFederationBasic.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var signInMessage =
                new SignInRequestMessage(new Uri(FederatedAuthentication.WSFederationAuthenticationModule.Issuer),
                    FederatedAuthentication.WSFederationAuthenticationModule.Realm);

            ViewBag.LoginUrl = signInMessage.WriteQueryString();

           
            return View();
        }

        public ActionResult Login()
        {
            var user = new User();

            return View(user);
        }
        [HttpPost]
        public ActionResult Login(User user)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(user.UserName, user.Password))
                {
                    var claims = new List<Claim>
                    {
                        new Claim("LoveProgramming","true"),
                        new Claim("Age","23") ,
                        new Claim(ClaimTypes.Name,user.UserName)
                    };


                    var identity = new ClaimsIdentity(claims, "forms");

                    var principal = new ClaimsPrincipal(identity);

                    var transformer =
                        FederatedAuthentication.FederationConfiguration.IdentityConfiguration
                            .ClaimsAuthenticationManager;

                    transformer.Authenticate("", principal);

                }

                return RedirectToAction("Index");
            }

            return View(user);
        }

        public ActionResult SignOut()
        {
            FederatedAuthentication.WSFederationAuthenticationModule.SignOut(isIPRequest:false);
            var signOutMessage =
                new SignOutRequestMessage(new Uri(FederatedAuthentication.WSFederationAuthenticationModule.Issuer),
                    FederatedAuthentication.WSFederationAuthenticationModule.Realm);
            return new RedirectResult(signOutMessage.WriteQueryString());
        }
        [ClaimsPrincipalPermission(SecurityAction.Demand,Operation = "View",Resource = "Protected")]
        public ActionResult Protected()
        {
            return View();
        }
         [Authorize]
        public ActionResult Protected2()
        {
           
            
            return View();
        }
    }
}