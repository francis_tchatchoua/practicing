﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;

namespace WsFederationBasic
{
    public class ClaimTransformer:ClaimsAuthenticationManager
    {
        public override ClaimsPrincipal Authenticate(string resourceName, ClaimsPrincipal incomingPrincipal)
        {
            if (incomingPrincipal.Identity.IsAuthenticated)
            {
                var claims = incomingPrincipal.Claims;



                var newclaims = new List<Claim>
                {
                    new Claim("FooClaim", "New claim1"),
                    new Claim("http://test.com/profile","View,Protected")
                };

                var finalClaims = claims.Union(newclaims);

                var identity = new ClaimsIdentity(finalClaims,"form");

                var principal = new ClaimsPrincipal(identity);

                EstablishSessionToken(principal);
                return principal;
            }
            
                return base.Authenticate(resourceName, incomingPrincipal);
           
        }

        private void EstablishSessionToken(ClaimsPrincipal principal)
        {

            var token = new SessionSecurityToken(principal);


            FederatedAuthentication.SessionAuthenticationModule.WriteSessionTokenToCookie(token);
        }
    }
}