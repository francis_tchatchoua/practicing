﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WsFederationBasic
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            FederatedAuthentication.SessionAuthenticationModule.SessionSecurityTokenReceived += SessionAuthenticationModule_SessionSecurityTokenReceived;
            FederatedAuthentication.FederationConfigurationCreated += FederatedAuthentication_FederationConfigurationCreated;
            FederatedAuthentication.WSFederationAuthenticationModule.RedirectingToIdentityProvider += WSFederationAuthenticationModule_RedirectingToIdentityProvider;
        }

        void WSFederationAuthenticationModule_RedirectingToIdentityProvider(object sender, RedirectingToIdentityProviderEventArgs e)
        {
            var message = e.SignInRequestMessage;

            //create sign in request message
            var m = new SignInRequestMessage(new Uri("https://mysts/wsfed"), "http://myapp");

            //create sign in link url
            var link = m.WriteQueryString();

            //changing the home realm
            e.SignInRequestMessage.HomeRealm = "web";
        }

        void FederatedAuthentication_FederationConfigurationCreated(object sender, System.IdentityModel.Services.Configuration.FederationConfigurationCreatedEventArgs e)
        {
            //create dynamically in code
        }

        void SessionAuthenticationModule_SessionSecurityTokenReceived(object sender, SessionSecurityTokenReceivedEventArgs e)
        {
            var token = e.SessionToken;

            
            var sam = sender as SessionAuthenticationModule;

            if (sam != null)
            {
                //e.SessionToken = sam.CreateSessionSecurityToken(token.ClaimsPrincipal, "", DateTime.Now,
                //   DateTime.Now.AddHours(2), false);

                //e.ReissueCookie = true;
            }
               
        }

    }
}
