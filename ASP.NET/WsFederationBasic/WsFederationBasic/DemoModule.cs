﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;

namespace WsFederationBasic
{
    public class DemoModule:IHttpModule
    {
        public void Dispose()
        {
            
        }

        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += context_AuthenticateRequest;
            context.PostAuthenticateRequest += context_PostAuthenticateRequest;
            context.BeginRequest += context_BeginRequest;
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            
        }

        void context_PostAuthenticateRequest(object sender, EventArgs e)
        {
            
        }

        void context_AuthenticateRequest(object sender, EventArgs e)
        {
            
        }
    }
}