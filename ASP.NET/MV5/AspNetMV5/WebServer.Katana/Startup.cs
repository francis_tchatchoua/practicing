﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebServer.Katana.Startup))]
namespace WebServer.Katana
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
