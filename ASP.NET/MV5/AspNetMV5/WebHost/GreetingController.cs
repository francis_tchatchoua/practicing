﻿using System.Web.Http;

namespace WebHost
{
   public class GreetingController:ApiController
    {
       public Greeting Get()
       {
           return new Greeting(){Text="Hello world"};
       }
    }
}
