﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin.Hosting;
using Owin;

namespace WebHost
{
    using AppFunc=Func<IDictionary<string,object>,Task>;
    class Program
     {
         static void Main(string[] args)
         {
             string uri = "http://localhost:3008";

             using (WebApp.Start<Startup>(uri))
             {
                 Console.WriteLine("started!");
                 Console.ReadKey();
                 Console.WriteLine("Stopping!");

             }
         }
     } 

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            /*app.UseWelcomePage();
            app.Run(ctx =>
            {
                return ctx.Response.WriteAsync("Hello world from katana host");
            });
             */

          /*  app.Use(async (environement, next) =>
            {
                foreach (var pair in environement.Environment)
                {
                    Console.WriteLine("{0}:{1}",pair.Key,pair.Value);
                }

                await next();
            });   */

            app.Use(async (environement, next) =>
            {
                Console.WriteLine("Requesting : " + environement.Request.Path);

                await next();

                Console.WriteLine("Response: " + environement.Response.StatusCode);
            });

            ConfigureWebApi(app);
            app.UserHelloWorld();
        }

        private void ConfigureWebApi(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            config.Routes.MapHttpRoute(
                "DefaultApi", 
                "api/{controller}/{id}",
                new {id = RouteParameter.Optional});

            app.UseWebApi(config);
        }
    }

    public static class AppBuilderExtension
    {
        public static void UserHelloWorld(this IAppBuilder app)
        {
            app.Use<HelloWorldComponent>();
        }
    }
    public class HelloWorldComponent
    {
        private AppFunc _next;

        public HelloWorldComponent(AppFunc next)
        {
            _next = next;
        }

        public  Task Invoke(IDictionary<string, object> environment)
        {
            var response = environment["owin.ResponseBody"] as Stream;

            using (var writer= new StreamWriter(response))
            {
                return writer.WriteAsync("Hello !!");
            }
          //  await _next(environment);
        }
    }
}
