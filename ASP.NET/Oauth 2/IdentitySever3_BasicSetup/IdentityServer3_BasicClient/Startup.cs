﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using Owin;
using IdentityServer3.AccessTokenValidation;
using Microsoft.Owin;

//[assembly: OwinStartup(typeof(IdentityServer3_BasicApi.Startup))]
namespace IdentityServer3_BasicApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            // accept access tokens from identityserver and require a scope of 'api1'
            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = "https://localhost:44333",
                ValidationMode = ValidationMode.ValidationEndpoint,
                RequiredScopes = new[] { "api1" }
               
                
            });

            // configure web api
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            // require authentication for all controllers
            config.Filters.Add(new AuthorizeAttribute());
            config.Routes.MapHttpRoute("apiDefault",
                "Api/{controller}/{id}",
                new { id = RouteParameter.Optional });

            app.UseWebApi(config);
        }
    }
}