﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using IdentityServer3.Core.Configuration;
using IdentityServer3.Core.Services.InMemory;
using Owin;

namespace IdentitySever3_BasicSetup
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
           
            var options = new IdentityServerOptions
            {
                Factory = new IdentityServerServiceFactory()
                            .UseInMemoryClients(Clients.Get())
                            .UseInMemoryScopes(Scopes.Get())
                            .UseInMemoryUsers(new List<InMemoryUser>()),
                
            };

            app.UseIdentityServer(options);
        }
    }
}
