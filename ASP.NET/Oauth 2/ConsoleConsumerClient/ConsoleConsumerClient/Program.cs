﻿using System;
using System.Net.Http;
using IdentityModel.Client;

namespace ConsoleConsumerClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var token = GetClientToken();

            CallApi(token);

            Console.Read();
        }

        static TokenResponse GetClientToken()
        {

            var client = new TokenClient(
                "https://localhost:44333/connect/token",
                "silicon",
                "F621F470-9731-4A25-80EF-67A6F7C5F4B8");

            

            
               

            return client.RequestClientCredentialsAsync("api1").Result;
        }

        static void CallApi(TokenResponse response)
        {
            var client = new HttpClient();
            client.SetBearerToken(response.AccessToken);

            Console.WriteLine(client.GetStringAsync("http://localhost:51431/api/test").Result);
        }
    }
}
