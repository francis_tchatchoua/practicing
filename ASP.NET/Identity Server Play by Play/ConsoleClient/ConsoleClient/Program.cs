﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using IdentityModel.Client;

namespace ConsoleClient
{
    class Program
    {
        static void Main()
        {
            var tokenResponse = GetToken().Result;
            var resultApiCall = CallApi(tokenResponse.AccessToken).Result;
            string jsonContent = resultApiCall.Content.ReadAsStringAsync().Result;

            Console.WriteLine(jsonContent);
            Console.Read();
        }

        private static async Task<TokenResponse> GetToken()
        {
            var client = new TokenClient(
                "https://localhost:44300/identity/connect/token",
                "mvc_service",
                "secret")
            {
                Timeout = TimeSpan.FromMinutes(1)
            };

            //application to application communication
            return await client.RequestClientCredentialsAsync("sampleApi");
        }
        private static async Task<HttpResponseMessage> CallApi(string token)
        {
            var client = new HttpClient()
            {
                BaseAddress = new Uri("http://localhost:60156")
            };
            var message= new HttpRequestMessage();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
            message.Headers.Add("Authorization",$"Bearer {token}"); 
            
            message.RequestUri= new Uri("http://localhost:60156/identity");
            var result = await client.SendAsync(message);


            return result;
        }
    }
}
