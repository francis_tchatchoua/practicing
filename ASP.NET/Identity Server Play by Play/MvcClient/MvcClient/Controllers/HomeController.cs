﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using Thinktecture.IdentityModel.Mvc;
using System.Net.Http;

namespace MvcClient.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult About()
        {
            return View((User as ClaimsPrincipal).Claims);
        }
        [ResourceAuthorize("Read", "ContactDetails")]
        [Auth(Roles = "Geek")]
        public ActionResult Contact()
        {
            return View();
        }
        [ResourceAuthorize("Write", "ContactDetails")]
        [HandleForbidden]
        public ActionResult UpdateContact()
        {
            if (!HttpContext.CheckAccess("Write", "ContactDetails", "some more data"))
            {
                // either 401 or 403 based on authentication state
                return this.AccessDenied();
            }

            ViewBag.Message = "Update your contact details!";

            return View();
        }
        public ActionResult Logout()
        {
            Request.GetOwinContext().Authentication.SignOut();
            return Redirect("/");
        }

        public ActionResult RequestFromApi()
        {
            var token = await GetTokenAsync().Result;

            var results = CallApi(token.AccessToken);
            ViewBag.JsonData = results;
            return View();
        }


        private async Task<TokenResponse> GetTokenAsync()
        {
            var client = new TokenClient(
                "https://localhost:44300/identity/connect/token",
                "mvc_service",
                "secret")
                {
                    Timeout = TimeSpan.FromMinutes(1)
                };

            //application to application communication
            return client.RequestClientCredentialsAsync("sampleApi").Result;
        }

        private async Task<string> CallApi(string token)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authentication", $"Bearer {token}");
            //client.SetBearerToken(token);
            var json = await client.GetStringAsync("http://localhost:60156/identity");
            return JArray.Parse(json).ToString();
        }
    }
}