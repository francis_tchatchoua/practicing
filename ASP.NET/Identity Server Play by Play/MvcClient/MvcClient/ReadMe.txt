﻿To confifure OpenId connect we need the following two packages:
***************************************************************
Microsoft.Owin.Security.Cookies
Microsoft.Owin.Security.OpenIdConnect

The package below is required for authentication
*************************************************
Thinktecture.IdentityModel.Owin.ResourceAuthorization.Mvc

This package is needed for google authentication
************************************************
Microsoft.Owin.Security.Google
