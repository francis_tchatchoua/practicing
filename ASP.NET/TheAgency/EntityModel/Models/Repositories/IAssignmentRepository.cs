﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheAgency.Models
{
    public interface IAssignmentRepository : IDisposable
	{
        Assignment Get(string agentId, DateTime date);
        IEnumerable<Assignment> GetAll(string agentId);
        Assignment AddOrUpdate(string agentId, Assignment assignment);
	}
}
