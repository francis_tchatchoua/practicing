﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace TheAgency.Models
{
    public class AssignmentRepository : IAssignmentRepository
	{
		private AgencyManagerContext _database = new AgencyManagerContext();

        public Assignment Get(string agentId, DateTime date)
		{
			var query = from agent in _database.Agents
                        from obs in agent.Assignments
						where agent.Id == agentId && obs.Date == date
						select obs;

            Assignment assignment = query.FirstOrDefault();		

			return assignment;
		}

        public IEnumerable<Assignment> GetAll(string agentId)
		{
			var query = from agent in _database.Agents
                        from assignment in agent.Assignments
						where agent.Id == agentId
                        select assignment;
			
			return query.ToList();

		}

        public Assignment AddOrUpdate(string agentId, Assignment assignment)
		{
            // Check if assignment already exists
			Agent agent =
                (from a in _database.Agents.Include(a => a.Assignments)
				where a.Id == agentId
				select a).FirstOrDefault();

            var existingAssignment = agent.Assignments.FirstOrDefault(o => o.Date == assignment.Date);

            if (existingAssignment == null)
			{
                // New, just take the assignment object
                existingAssignment = assignment;
                agent.Assignments.Add(existingAssignment);				
			}
			else
			{
                // Existing, update the assignment object
                existingAssignment.Description = assignment.Description;
                existingAssignment.Priority = assignment.Priority;				
			}						
						
			_database.SaveChanges();

            return existingAssignment;
		}

        public void Dispose()
        {
            _database.Dispose();
        }
    }
}