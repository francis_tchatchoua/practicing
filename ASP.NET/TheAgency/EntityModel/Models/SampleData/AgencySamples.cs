﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;

namespace TheAgency.Models
{
    public static class ConcurrentDictionaryExtender
    {
        public static void AddOrUpdate<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            dictionary.AddOrUpdate(key, value, (s, val) => val);
        }

    }

    public class AgencySamples
    {
        private static ConcurrentDictionary<string, Agent> _agents = new ConcurrentDictionary<string, Agent>();
        private static ConcurrentDictionary<int, Assignment> _assignments = new ConcurrentDictionary<int, Assignment>();

        static AgencySamples()
        {
            // Initialize the in-memory store once

            _agents.AddOrUpdate("blond", new Agent { Id = "Blond", Alias = "Sweeper", FullName = "Jake Blond", Version = 1, Assignments = new List<Assignment>() });
            _agents.AddOrUpdate("forces", new Agent { Id = "Forces", Alias = "Danger", FullName = "Alistair Forces", Version = 1, Assignments = new List<Assignment>() });
            _agents.AddOrUpdate("bjorn", new Agent { Id = "Bjorn", Alias = "Alpha-One", FullName = "Jamie Bjorn", Version = 1, Assignments = new List<Assignment>() });
            _agents.AddOrUpdate("london", new Agent { Id = "London", Alias = "Nightingale", FullName = "Cindy London", Version = 1, Assignments = new List<Assignment>() });
            _agents.AddOrUpdate("french", new Agent { Id = "French", Alias = "Stealthy", FullName = "Johnny French", Version = 1, Assignments = new List<Assignment>() });

            _assignments.AddOrUpdate(1, new Assignment { Id = 1, Date = new DateTime(2014, 12, 4), Description = "Locate nuke", Priority = Priorities.RedAlert });
            _assignments.AddOrUpdate(2, new Assignment { Id = 2, Date = new DateTime(2014, 11, 23), Description = "Learn chinese", Priority = Priorities.High });
            _assignments.AddOrUpdate(3, new Assignment { Id = 3, Date = new DateTime(2015, 1, 4), Description = "Test the new car", Priority = Priorities.Low });
            _assignments.AddOrUpdate(4, new Assignment { Id = 4, Date = new DateTime(2015, 2, 20), Description = "Buy ammunition", Priority = Priorities.High });
            _assignments.AddOrUpdate(5, new Assignment { Id = 5, Date = new DateTime(2015, 2, 1), Description = "Stop the assassin", Priority = Priorities.RedAlert });
            _assignments.AddOrUpdate(6, new Assignment { Id = 6, Date = new DateTime(2014, 11, 2), Description = "Walk the dog", Priority = Priorities.Medium });
            _assignments.AddOrUpdate(7, new Assignment { Id = 7, Date = new DateTime(2014, 10, 30), Description = "Buy milk", Priority = Priorities.Medium });

            _agents["blond"].Assignments.Add(_assignments[1]);
            _agents["forces"].Assignments.Add(_assignments[2]);
            _agents["london"].Assignments.Add(_assignments[3]);
            _agents["bjorn"].Assignments.Add(_assignments[4]);
            _agents["forces"].Assignments.Add(_assignments[5]);
            _agents["blond"].Assignments.Add(_assignments[6]);
            _agents["french"].Assignments.Add(_assignments[7]);
        }

        public static ConcurrentDictionary<string, Agent> Agents
        {
            get
            {
                return _agents;
            }
        }

        public static ConcurrentDictionary<int, Assignment> Assignments
        {
            get
            {
                return _assignments;
            }
        }



    }
}