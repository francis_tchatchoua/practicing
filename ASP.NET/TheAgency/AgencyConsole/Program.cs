﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Reflection;

namespace AgencyConsole
{    
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            string serverAddress = "https://localhost:44304";

            ServicePointManager.ServerCertificateValidationCallback = AutomaticCertificateApprover;
            UploadPhotoOfAgent(serverAddress).Wait();            
            //PrintAgentsList(serverAddress).Wait();            
        }

        static private bool AutomaticCertificateApprover(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
     
        private async static Task UploadPhotoOfAgent(string serverAddress)
        {
            using (var proxy = new HttpClient() { BaseAddress = new Uri(serverAddress) })
            {
                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                using (FileStream fileStream = new FileStream(Path.Combine(path, "blond-real.jpg"), FileMode.Open))
                {
                    // Create a stream content for the file
                    StreamContent content = new StreamContent(fileStream);

                    // Post the MIME multipart form data upload with the file
                    HttpResponseMessage response = await proxy.PostAsync("api/agents/blond/imageStream", content);

                    Console.WriteLine(response.StatusCode);

                    Console.WriteLine("Press Enter to close.");
                    Console.ReadLine();
                }
            }
        }

        private async static Task PrintAgentsList(string serverAddress)
        {
            // To use ADFS, you need to call Add-ADFSClient cmdlet in the ADFS server
            // Add-ADFSClient -Name "MyClient" -ClientId "E1CF1107-FF90-4228-93BF-26052DD2C714" -RedirectUri "http://anarbitraryreturnuri/"
            string authority = "https://adfs.idoflatow.net/adfs";
            string resource = "http://localhost/theagency";
            string clientId = "E1CF1107-FF90-4228-93BF-26052DD2C714";
            string redirectUrl = "http://anarbitraryreturnuri/";

            AuthenticationContext authenticationContext =
                new AuthenticationContext(authority, validateAuthority: false);

            AuthenticationResult authenticationResult =
              authenticationContext.AcquireToken(
                    resource,
                    clientId,
                    new Uri(redirectUrl));

            string authHeader = authenticationResult.CreateAuthorizationHeader();

            using (var client = new HttpClient() { BaseAddress = new Uri(serverAddress) })
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "api/agents");
                request.Headers.TryAddWithoutValidation("Authorization", authHeader);
                HttpResponseMessage agentsResponse = await client.SendAsync(request);

                agentsResponse.EnsureSuccessStatusCode();

                // Since this response is not part of the domain model, 
                // we create an anonymous type for it					                
                var agentsSummary = await agentsResponse.Content.ReadAsAsync<IEnumerable<AgentBrief>>();
                foreach (var agentSummary in agentsSummary)
                {
                    // Since we only got a summary, we'll get the full details
                    // of each agent
                    HttpRequestMessage agentRequest = new HttpRequestMessage(HttpMethod.Get, agentSummary.Link);
                    agentRequest.Headers.TryAddWithoutValidation("Authorization", authHeader);
                    
                    HttpResponseMessage agentResponse = await client.SendAsync(agentRequest);
                    agentResponse.EnsureSuccessStatusCode();
                    
                    Agent agent = await agentResponse.Content.ReadAsAsync<Agent>();
                    Console.WriteLine(
                        string.Format("Id: {0}\nFull Name: {1}",
                            agent.Id, agent.FullName));
                }

                Console.WriteLine("Press Enter to close.");
                Console.ReadLine();
            }
        }
    }

    // We can define a subset of the real domain model
    // in case we do not require all the information
    public class AgentBrief
    {
        public string FullName { get; set; }
        public string Link { get; set; }
    }

    public class Agent
    {
        public string Id { get; set; }
        public string FullName { get; set; }
    }
}
