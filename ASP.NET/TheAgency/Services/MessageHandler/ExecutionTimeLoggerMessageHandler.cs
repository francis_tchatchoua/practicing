﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace Services.MessageHandler
{
    public class ExecutionTimeLoggerMessageHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);
            stopWatch.Stop();

            // Add the execution time to the response header 
            // And write it to trace
            response.Headers.Add("X-ExecutionTime", stopWatch.ElapsedMilliseconds.ToString());
            
            request.GetConfiguration().Services.GetTraceWriter().Info(
                request, "Timing", 
                "Ellapsed milliseconds for request {0}: {1}", 
                request.RequestUri, stopWatch.ElapsedMilliseconds);

            return response;
        }

      
    }
}
