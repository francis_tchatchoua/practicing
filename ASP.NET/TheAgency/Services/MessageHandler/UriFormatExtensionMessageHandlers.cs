﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Threading.Tasks;

namespace TheAgency.MessageHandlers
{
    public class UriFormatExtensionMessageHandler : DelegatingHandler
    {
        static IDictionary<string, string> extensionMappings = new Dictionary<string, string>()
            {
                { "jpg", "image/jpg" },
                { "csv", "text/csv" },
            };
                
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            var path = request.RequestUri.AbsolutePath;						
            var ext = path.Substring(path.LastIndexOf('.') + 1);
            string mediaType = null;
            var found = extensionMappings.TryGetValue(ext, out mediaType);
            
            if (found)
            {
				// Replace the url xxx.ext with url xxx and content type = ext
                var newUri = request.RequestUri.OriginalString.Replace('.' + ext, String.Empty);
                request.RequestUri = new Uri(newUri, UriKind.Absolute);
				if (request.GetRouteData() != null)
					request.GetRouteData().Values.Remove("ext");
                request.Headers.Accept.Clear();
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));
            }

            return base.SendAsync(request, cancellationToken);
        }		
    }
}