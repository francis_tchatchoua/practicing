﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using TheAgency.Models;

namespace Services.Results
{
    public class AgentWithETagResponse : IHttpActionResult
    {
        Agent _agent;
        HttpRequestMessage _request;

        public AgentWithETagResponse(Agent agent, HttpRequestMessage request)
        {
            _agent = agent;
            _request = request;
        }

        public Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            HttpResponseMessage response;

            // Check if agent was found
            if (_agent == null)
            {
                response = _request.CreateResponse(HttpStatusCode.NotFound);
            }
            else
            {
                // Check if etag (version) has changed
                if (GetETag() == _agent.Version.ToString())
                {
                    response = _request.CreateResponse(HttpStatusCode.NotModified);
                }
                else
                {
                    response = _request.CreateResponse(HttpStatusCode.OK, _agent);
                    SetETag(response, _agent.Version.ToString());
                }

            }
            return Task.FromResult(response);
        }



        string GetETag()
        {
            var etag = _request.Headers.IfNoneMatch.FirstOrDefault();
            string etagValue;

            if (etag != null)
            {
                etagValue = etag.ToString().Replace(@"""", "");
            }
            else
            {
                etagValue = null;
            }

            return etagValue;
        }

        void SetETag(HttpResponseMessage response, string value)
        {
            response.Headers.ETag = new EntityTagHeaderValue(
                  string.Format(@"""{0}""", value));
        }
    }
}
