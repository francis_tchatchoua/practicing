﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;

namespace Services.Services
{
    public class Log4NetExceptionLogger : ExceptionLogger
    {      
        public override void Log(ExceptionLoggerContext context)
        {
            ILog log = LogManager.GetLogger(context.ExceptionContext.ControllerContext.Controller.GetType());
            log.ErrorFormat("Unhandled exception processing {0} for {1}: {2}",
                context.Request.Method,
                context.Request.RequestUri,
                context.Exception);            
        }
    }
}
