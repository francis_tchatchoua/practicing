﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TheAgency.Models;
using System.Web.Http.ModelBinding;
using System.Web.Http.ModelBinding.Binders;
using TheAgency.Filters;

namespace TheAgency.APIControllers
{
    [RoutePrefix("api/agents/{agentId}/assignments")]
    public class AssignmentsController : ApiController
    {
        private IAssignmentRepository _repository;

        public AssignmentsController(IAssignmentRepository repository)
        {
            _repository = repository;
        }

        [Route()]
        public IEnumerable<Assignment> Get(string agentId)
        {
            return _repository.GetAll(agentId);
        }

        [Route("{date}")]
        public Assignment Get(string agentId, DateTime date)
        {
            return _repository.Get(agentId, date);
        }

        [Route("{date}")]
        public HttpResponseMessage Put(string agentId, DateTime date,
            [FromBody] Assignment assignment)
        {
            assignment.Date = date;
            Assignment newAssignment = _repository.AddOrUpdate(agentId, assignment);

            HttpResponseMessage response = Request.CreateResponse<Assignment>(HttpStatusCode.OK, newAssignment);
            return response;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
