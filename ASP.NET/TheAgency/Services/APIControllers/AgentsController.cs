﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TheAgency.Models;
using TheAgency.Filters;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Web;
using System.Text;
using Newtonsoft.Json;
using System.Threading;
using Services.Results;

namespace TheAgency.ApiControllers
{
    //[Authorize(Roles="Agents")]
    public class AgentsController : ApiController
    {
        private IAgentRepository _repository;

        public AgentsController(IAgentRepository repository)
        {
            _repository = repository;
        }

        [CachingFilter(5)]
        public IHttpActionResult Get(string id)
        {
            Agent agent = _repository.Get(id);

            if (agent == null)
            {
                // You can also use the following syntax:
                return NotFound();
            }
            agent.Image = GetAgentLocation(id) + ".jpg";
            agent.ImageLink = GetAgentLocation(agent.Id) + "/imageForm";
            return new AgentWithETagResponse(agent, this.Request);
        }


        [AllowAnonymous]
        public List<AgentThin> Get()
        {
            return (from agent in _repository.GetAll().ToList()
                    select new AgentThin
                    {
                        FullName = agent.FullName,
                        Link = GetAgentLocation(agent.Id)
                    }).ToList();
        }

        [Route("api/agents/stream")]
        public HttpResponseMessage GetAsStream()
        {
            Request.Headers.AcceptEncoding.Clear();
            HttpResponseMessage response = Request.CreateResponse();

            string responseType = "application/octet-stream";

            response.Content = new PushStreamContent(
                (stream, headers, context) =>
                {
                    StreamWriter sWriter = new StreamWriter(stream);
                    StreamWriter sw = (StreamWriter)sWriter;

                    foreach (var agent in _repository.GetAll())
                    {
                        AgentThin thinAgent = new AgentThin
                        {
                            FullName = agent.FullName,
                            Link = GetAgentLocation(agent.Id)
                        };
                        StringBuilder sb = new StringBuilder();
                        TextWriter tw = new StringWriter(sb);
                        JsonSerializer js = new JsonSerializer();
                        js.Serialize(tw, thinAgent);
                        string jstring = sb.ToString();
                        sw.WriteLine(jstring);
                        sw.Flush();
                        Thread.Sleep(2000);
                    }

                    sw.Close();
                    sw.Dispose();
                },
                responseType);

            return response;
        }


        [ValidateFilter]
        [HttpPost]
        public IHttpActionResult CreateAgent(Agent agent)
        {
            agent = _repository.Add(agent);

            return Created(GetAgentLocation(agent.Id), agent);
        }

        [HttpPost]
        [Route("api/agents/{id}/imageStream")]
        public async Task<HttpResponseMessage> UploadImageFromStream(string id)
        {
            Stream imageStream = await Request.Content.ReadAsStreamAsync();
            string path;
            if (System.Web.HttpContext.Current == null)
            {
                // Self hosting
                path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            else
            {
                // IIS hosting
                path = HttpContext.Current.Server.MapPath("~");
            }
            string imagePath = Path.Combine(path, @"Images\Agents\", id + ".jpg");
            using (FileStream fs = new FileStream(imagePath, FileMode.Create))
            {
                await imageStream.CopyToAsync(fs);
                fs.Close();
            }
            imageStream.Close();

            // Change the version of the agent, so the client will reload its photo
            Agent agent = _repository.Get(id);
            agent.Version++;
            _repository.Update(agent);

            var response = Request.CreateResponse(HttpStatusCode.Accepted);
            return response;
        }

        [HttpPost]
        [Route("api/agents/{id}/imageForm")]
        public async Task<HttpResponseMessage> UploadImageFromForm(string id)
        {
            string path;
            if (System.Web.HttpContext.Current == null)
            {
                // Self hosting
                path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            else
            {
                // IIS hosting
                path = HttpContext.Current.Server.MapPath("~");
            }

            string imagePath = Path.Combine(path, @"Images\Agents\");

            var streamProvider = new MultipartFormDataStreamProvider(imagePath);
            await Request.Content.ReadAsMultipartAsync(streamProvider);
            MultipartFileData fileData = streamProvider.FileData[0];
            string newFileName = Path.Combine(imagePath, id + ".jpg");
            if (File.Exists(newFileName))
            {
                File.Delete(newFileName);
            }
            File.Move(fileData.LocalFileName, newFileName);

            // Change the version of the agent, so the client will reload its photo
            Agent agent = _repository.Get(id);
            agent.Version++;
            _repository.Update(agent);

            var response = Request.CreateResponse(HttpStatusCode.Accepted);
            return response;
        }



        private Uri GetAgentLocation(string agentId)
        {
            var controller = this.ControllerContext.ControllerDescriptor.ControllerName;
            return new Uri(this.Url.Link("AgencyAPI", new { controller = controller, id = agentId }));
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    public class AgentThin
    {
        public string FullName { get; set; }
        public Uri Link { get; set; }
    }
}