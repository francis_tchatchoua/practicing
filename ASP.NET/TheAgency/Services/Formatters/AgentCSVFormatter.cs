﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TheAgency.ApiControllers;
using TheAgency.Models;

namespace TheAgency.Formatters
{
    public class AgentCSVFormatter : BufferedMediaTypeFormatter
    {
        public AgentCSVFormatter()
        {
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/csv"));            
            this.AddUriPathExtensionMapping("csv", new MediaTypeHeaderValue("text/csv"));
        }

        public override void WriteToStream(Type type, object value, Stream writeStream, System.Net.Http.HttpContent content)
        {
            var agents = value as IEnumerable<AgentThin>;            
            using (var writer = new StreamWriter(writeStream))            
            {
                // Write CSV header
                writer.WriteLine("Full Name, Link");
                if (agents != null)
                {
                    foreach (var agent in agents)
                    {
                        writer.WriteLine("{0},{1}", agent.FullName,agent.Link);
                    }
                }              
            }

        }

        public override bool CanWriteType(Type type)
        {
            Type enumerableType = typeof(IEnumerable<AgentThin>);
            return enumerableType.IsAssignableFrom(type);
        }
        public override bool CanReadType(Type type)
        {
            return false;
        }   
    }
}
