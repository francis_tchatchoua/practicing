﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Linq;
using System.Threading.Tasks;
using TheAgency.Models;
using System.Web;
using System.Reflection;

namespace TheAgency.Formatters
{
	public class AgentImageFormatter : MediaTypeFormatter
	{
		public AgentImageFormatter()
		{
			this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("image/jpg"));
            this.AddUriPathExtensionMapping("jpg", new MediaTypeHeaderValue("image/jpg"));
		}
        public override async Task WriteToStreamAsync(Type type, object value, Stream writeStream, System.Net.Http.HttpContent content, TransportContext transportContext)
        {            
			var agent = value as Agent;
			if (agent != null)
			{
				string path;
				if (System.Web.HttpContext.Current == null)
				{
					// Self hosting
					path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
				}
				else
				{
					// IIS hosting
					path = HttpContext.Current.Server.MapPath("~");
				}
				string imagePath = Path.Combine(path, @"Images\Agents\", agent.Id + ".jpg");

				using (var fileStream = new FileStream(imagePath, FileMode.Open))
				{
                    await fileStream.CopyToAsync(writeStream);
				}
			}
		}

		public override bool CanReadType(Type type)
		{
			return false;
		}

		public override bool CanWriteType(Type type)
		{
			return typeof(Agent).Equals(type);
		}
	}
}
