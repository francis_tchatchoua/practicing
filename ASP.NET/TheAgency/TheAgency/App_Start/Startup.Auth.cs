﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.ActiveDirectory;
using Owin;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin;
using Microsoft.Owin.Security.WsFederation;
using Microsoft.Owin.Security.Cookies;
using System.IdentityModel.Tokens;
using System.Threading.Tasks;
using System.Security.Claims;

namespace TheAgency
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Default will be cookie authentication, created by MVC sign-in
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            app.UseCookieAuthentication(new CookieAuthenticationOptions());

            // Use WS-FED with ADFS for the MVC passive client
            app.UseWsFederationAuthentication(
               new WsFederationAuthenticationOptions
               {
                   MetadataAddress = ConfigurationManager.AppSettings["ida:FederationMetadataLocation"],
                   Wtrealm = ConfigurationManager.AppSettings["ida:Realm"]
               });

            // Use OAuth with ADFS 3.0 for the Web API service (direct calls with active clients)
            app.UseActiveDirectoryFederationServicesBearerAuthentication(
                new ActiveDirectoryFederationServicesBearerAuthenticationOptions
                {
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidAudience = ConfigurationManager.AppSettings["ida:Audience"]
                    },
                    MetadataEndpoint = ConfigurationManager.AppSettings["ida:FederationMetadataLocation"],
                });

        }
    }
}
