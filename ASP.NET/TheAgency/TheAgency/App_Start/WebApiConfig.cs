﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Ninject;
using TheAgency.Models;
using TheAgency.Formatters;
using TheAgency.Filters;
using Services.MessageHandler;
using System.Net.Http.Formatting;
using Services.Services;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Tracing;

namespace TheAgency
{
	public class WebApiConfig
	{
        public static void Configure(HttpConfiguration config)
        {
			var kernel = new StandardKernel();
			kernel.Bind<IAgentRepository>().To<AgentRepository>();
            kernel.Bind<IAssignmentRepository>().To<AssignmentRepository>();			
			Configure(config, kernel);
        }
	
		private static void Configure(HttpConfiguration config, IKernel kernel)
		{			
			// Define IoC
            config.DependencyResolver = new NinjectDependencyResolver(kernel);

            // Suppressing host authentication will prevent using cookie authentication
            // when calling Web API from JavaScript
       //     config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            config.MapHttpAttributeRoutes();
		
			config.Routes.MapHttpRoute(
				name: "AgencyAPIIdExt", 
				routeTemplate: "api/{controller}/{id}.{ext}");

            config.Routes.MapHttpRoute(
                name: "AgencyAPIControllerExt",
                routeTemplate: "api/{controller}.{ext}/{id}",
                defaults: new { id = RouteParameter.Optional });

			config.Routes.MapHttpRoute(
			    name: "AgencyAPI",
			    routeTemplate: "api/{controller}/{id}",
			    defaults: new { id = RouteParameter.Optional });
          
			// Define formatters			
			config.Formatters.Add(new AgentImageFormatter());            
            config.Formatters.Add(new AgentCSVFormatter());            

            // This is some configuration for easier display of demos
			config.Formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
			var enumConverter = new Newtonsoft.Json.Converters.StringEnumConverter();
			config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(enumConverter);
			config.Formatters.XmlFormatter.Indent = true;

            // Configure exception logger and handler
            config.Services.Replace(typeof(IExceptionHandler), new GenericExceptionHandler());
            config.Services.Add(typeof(IExceptionLogger), new Log4NetExceptionLogger());

            SystemDiagnosticsTraceWriter tracer = config.EnableSystemDiagnosticsTracing();
            config.MessageHandlers.Add(new ExecutionTimeLoggerMessageHandler());
		}
	}
}