﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;
using LocalizationLibrary;

namespace LocalisationDemo.Models
{
    public class LoginModel
    {
        [Required(ErrorMessageResourceType = typeof(MyResources),ErrorMessageResourceName = @"UsernameRequired")]
        [Display(Name = "Username", ResourceType = typeof(MyResources))]
        public string   UserName { get; set; }
        [Required(ErrorMessageResourceType = typeof(MyResources), ErrorMessageResourceName = @"PasswordRequired")]
        [Display(Name = "Password", ResourceType = typeof(MyResources))]
        public string Password { get; set; }
    }
}