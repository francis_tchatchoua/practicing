﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using AuthenticationPipeLine.Authentication;
using AuthenticationPipeLine.Middleware;
using AuthenticationPipeLine.Middleware.CliamTransformation;
using Owin;
using System.Threading.Tasks;

namespace AuthenticationPipeLine
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var configuration = new HttpConfiguration();

            configuration.Routes.MapHttpRoute(
                "default",
                "api/{controller}/{id}",
                new {id=RouteParameter.Optional});

            //configuration.Filters.Add(new TestAuthenticationFilter());
            app.Use(typeof(TestMiddleware));
            app.UseClaimTransformation(Transformation);
            
            app.UseWebApi(configuration);
            

        }

        private async Task<ClaimsPrincipal> Transformation(ClaimsPrincipal incominPrincipal)
        {
            if (!incominPrincipal.Identity.IsAuthenticated)
            {
                return incominPrincipal;
            }

            var name = incominPrincipal.Identity.Name;

            //go to database and fetch user data
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, incominPrincipal.Identity.Name),
                new Claim(ClaimTypes.Role, "best"),
                new Claim(ClaimTypes.Email, "foo@foo")
            };

            var id = new ClaimsIdentity("custom");

            id.AddClaims(claims);

            return new ClaimsPrincipal(id);
        }
    }
}