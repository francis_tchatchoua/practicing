﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace AuthenticationPipeLine.Authentication
{
    public class TestAuthorizationFilerAttribute  :AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            Debug.WriteLine("inside the authorization filter {0} \n",actionContext.RequestContext.Principal.Identity.Name);
            //return base.IsAuthorized(actionContext);

            return true;
        }
    }
}