﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace AuthenticationPipeLine.Authentication
{
    public class TestAuthenticationFilterAttribute:Attribute,IAuthenticationFilter
    {
        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            Debug.WriteLine("inside the authentication filter: {0} \n",context.ActionContext.RequestContext.Principal.Identity.Name);

        }

        public async Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            
        }

        public bool AllowMultiple
        {
            get { return false; }
        }
    }
}