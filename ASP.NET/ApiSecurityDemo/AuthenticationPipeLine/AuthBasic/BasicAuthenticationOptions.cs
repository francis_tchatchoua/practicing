﻿using Microsoft.Owin.Security;

namespace AuthenticationPipeLine.AuthBasic
{
    public class BasicAuthenticationOptions :AuthenticationOptions
    {
        public string Realm { get; private set; }
        public BasicAuthenticationMiddleware.CredentionValidationFunction ValidationFunction { get; set; }
        public BasicAuthenticationOptions(string realm,BasicAuthenticationMiddleware.CredentionValidationFunction validationFunction) : base("Basic")
        {
            Realm = realm;
            ValidationFunction = validationFunction;
        }
    }
}