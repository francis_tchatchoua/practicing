﻿using System.Diagnostics;
using System.Web.Http;
using AuthenticationPipeLine.Authentication;

namespace AuthenticationPipeLine
{
    public class TestController :ApiController
    {
        [TestAuthorizationFiler]
        [TestAuthenticationFilter]
        public IHttpActionResult Get()
        {
            var name = "";
            if (User != null)
            {
                name = User.Identity.Name;
            }
            Debug.Write("inside the test controller Get method {0} \n",name);
            return Ok();
        }
    }
}