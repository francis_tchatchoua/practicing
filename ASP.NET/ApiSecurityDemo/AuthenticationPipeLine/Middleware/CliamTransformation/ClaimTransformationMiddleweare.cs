﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

namespace AuthenticationPipeLine.Middleware.CliamTransformation
{
    public class ClaimTransformationMiddleweare
    {
        private Func<IDictionary<string, object>, Task> _nextFunc;
        private ClaimTransformationOptions _transformationOptions;

        public ClaimTransformationMiddleweare(Func<IDictionary<string,object>,Task> nextFunc,ClaimTransformationOptions transformationOptions )
        {
            _transformationOptions = transformationOptions;
            _nextFunc = nextFunc;
        }

        public async Task Invoke(IDictionary<string, object> env)
        {
            var context = new OwinContext(env);

            if (context.Authentication != null && context.Authentication.User != null)
            {
                var transformedPrincipal = await _transformationOptions.ClaimsTransformer(context.Authentication.User);
                context.Authentication.User = transformedPrincipal;
            }

            await _nextFunc(env);
        }
    }
}