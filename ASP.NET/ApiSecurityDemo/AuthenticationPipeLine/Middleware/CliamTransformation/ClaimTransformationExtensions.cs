﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Owin;

namespace AuthenticationPipeLine.Middleware.CliamTransformation
{
    public static class ClaimTransformationExtensions
    {
        public static IAppBuilder UseClaimTransformation(this IAppBuilder app, Func<ClaimsPrincipal, Task<ClaimsPrincipal>> transformer)
        {
            return app.UseClaimTransformation(new ClaimTransformationOptions()
            {
                ClaimsTransformer = transformer
            });
        }
        public static IAppBuilder UseClaimTransformation(this IAppBuilder app, ClaimTransformationOptions options)
        {
            if (options == null)
            {
                throw  new ArgumentNullException("options");
            }

            app.Use(typeof (ClaimTransformationMiddleweare), options);

            return app;
        }
    }
}