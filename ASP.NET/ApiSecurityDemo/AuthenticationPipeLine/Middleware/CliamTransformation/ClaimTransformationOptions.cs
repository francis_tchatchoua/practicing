﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuthenticationPipeLine.Middleware.CliamTransformation
{
    public class ClaimTransformationOptions
    {
        public Func<ClaimsPrincipal, Task<ClaimsPrincipal>> ClaimsTransformer;

    }
}