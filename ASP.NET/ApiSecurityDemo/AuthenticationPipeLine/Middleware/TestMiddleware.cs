﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.Owin;

namespace AuthenticationPipeLine.Middleware
{
    public class TestMiddleware
    {
        private Func<IDictionary<string, object>, Task> _next;

        public TestMiddleware(Func<IDictionary<string,object>,Task> next)
        {
            _next = next;
        }

        public async Task Invoke(IDictionary<string,object>  env)
        {

            var context = new OwinContext(env);

           

            //simulate authentication
            context.Request.User=new GenericPrincipal(new GenericIdentity("francis"),new string[]{"foo"} );
            Debug.WriteLine("inside the test middleware: {0} \n", context.Request.User.Identity.Name);
            await _next(env);
        }
    }
}