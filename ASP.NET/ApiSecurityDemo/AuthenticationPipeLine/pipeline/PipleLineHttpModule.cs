﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace AuthenticationPipeLine.pipeline
{
    public class PipleLineHttpModule:IHttpModule
    {
        public void Dispose()
        {
           
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            var name = "";
            if (HttpContext.Current.User != null)
            {
                name = HttpContext.Current.User.Identity.Name;
            }
            Debug.Write("http module begin request {0} \n",name);
        }
    }
}