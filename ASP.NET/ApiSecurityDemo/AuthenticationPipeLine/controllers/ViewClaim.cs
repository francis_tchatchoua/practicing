﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AuthenticationPipeLine.controllers
{
    public class ViewClaim
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
