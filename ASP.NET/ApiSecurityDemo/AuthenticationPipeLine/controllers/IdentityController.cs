﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;

namespace AuthenticationPipeLine.controllers
{                                    
    public class IdentityController  :ApiController
    {
        public IEnumerable<ViewClaim> Get()
        {
            var principal = User as ClaimsPrincipal;

            if (principal == null) return null;

            return principal.Claims.Select(c => new ViewClaim()
            {
                Type = c.Type,
                Value = c.Value
            });
        }
    }
}