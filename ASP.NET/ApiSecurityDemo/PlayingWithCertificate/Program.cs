﻿using System;
using System.IdentityModel.Selectors;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Thinktecture.IdentityModel;

namespace PlayingWithCertificate
{
    class Program
    {
        static void Main(string[] args)
        {
            //pull certificate from local machine in personal certificate folder
          /*  var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);

            store.Open(OpenFlags.ReadOnly);

            var certificates = store.Certificates.Find(X509FindType.FindBySubjectDistinguishedName, "‎CN=web.local", true);
            
            */
            //We can also use thinktechture.identityModel.Core to find certicate

            var certificate = X509.LocalMachine.My.SubjectDistinguishedName.Find("CN=web.local").First();

            //how to validate a certificate using chain policy

            var chain = new X509Chain();

            var policy = new X509ChainPolicy()
            {
                RevocationFlag = X509RevocationFlag.EntireChain,    //check the entire certicate chain
                RevocationMode = X509RevocationMode.Offline , // do not go online to the certificate authority to check the revocation list.
                VerificationFlags = X509VerificationFlags.IgnoreInvalidBasicConstraints
            };


            chain.ChainPolicy = policy;

            if (!chain.Build(certificate))
            {
                foreach (var el in chain.ChainElements)
                {
                    foreach (var status in el.ChainElementStatus)
                    {
                        Console.WriteLine(status.StatusInformation);
                    }
                }
            }

            //another way of validating your cerficate
            var validator = X509CertificateValidator.ChainTrust;

            validator.Validate(certificate);   //this throw an exception if we have a validation error
            Console.Read();

            //NB: Always validate the certificate before accessing any property

            //Get the httpconfig tool here : http://www.stevestechspot.com/ABetterHttpcfg.aspx
        }
    }
}
