﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = {3, 3, 8, 3, 9, 4, 7, 8, 33, 22};
            var result =solution(a);

            Console.WriteLine(result);

            Console.Read();
        }

        public static int solution(int[] A)
        {
            A= BubbleSort(A);

            int element = A[0];
            int count = 1;
            for (int i = 1; i < A.Length; i++)
            {
                if (element == A[i])
                    continue;
                else
                {
                    element = A[i];
                    count++;
                }
            }

            int[] result = new int[count];

            count = 0;
            element = A[0];
            result[count++] = element;
            for (int i = 1; i < A.Length; i++)
            {
                if (element == A[i])
                    continue;
                else
                {
                    element = A[i];
                    result[count++] = element;
                }
            }
            
            var distance = 0;

            if (result[0] > A[result.Length - 1]) distance = result[0] = result[A.Length - 1];
            else
            {
                distance = result[result.Length - 1] = result[0];
            }

            return distance;
        }

        static int[] BubbleSort(int[] numarray)
        {
            int max = numarray.Length;
            for (int i = 1; i < max; i++)
            {
                for (int j = 0; j < max - i; j++)
                {

                    if (numarray[j] > numarray[j + 1])
                    {
                        int temp = numarray[j];
                        numarray[j] = numarray[j + 1];
                        numarray[j + 1] = temp;
                    }
                }
            }
            return numarray;
        }

    }
}
