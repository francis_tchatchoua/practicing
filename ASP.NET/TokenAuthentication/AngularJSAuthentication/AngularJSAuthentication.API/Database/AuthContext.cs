﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace AngularJSAuthentication.API.Database
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext() : base("AuthContext")
        {}
    }
}