﻿
Install-Package Microsoft.AspNet.WebApi.Owin -Version 5.1.2 (for enabling web api)
Install-Package Microsoft.Owin.Host.SystemWeb -Version 2.1.0 (this is used to enable our Owin server to run our API on IIS using ASP.NET request pipeline)

-----------------------------------------------------------------------------
Install-Package Microsoft.AspNet.Identity.Owin -Version 2.0.1 (enable asp.net identity)
Install-Package Microsoft.AspNet.Identity.EntityFramework -Version 2.0.1 (to use entity framework with asp.net identity)

-----------------------------------------------------------------------------------
Install-Package Microsoft.Owin.Security.OAuth -Version 2.1.0 (add support for oauth in owin)

----------------------------------------------------------------------------------
Install-Package Microsoft.Owin.Cors -Version 2.1.0 (enable cors to the app)