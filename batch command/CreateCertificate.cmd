makecert.exe ^
-iv DevRoot.pvk ^
-ic DevRoot.cer ^
-n "CN=%1" ^
-pe ^
-sv %1.pvk ^
-a sha1 ^
-len 2048 ^
-b 09/13/2015 ^
-e 09/13/2016 ^
-sky exchange ^
-eku 1.3.6.1.5.5.7.3.1 ^
%1.cer 

pvk2pfx.exe ^
-pvk %1.pvk ^
-spc %1.cer ^
-pfx %1.pfx
