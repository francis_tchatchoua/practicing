﻿$cool={Clear-Host; "Powershell is some funny language"}

#execute script block
& $cool

$value ={41+1}

&$value

1 + (&$value)

$foo={17; Write-Host "Powershell is some funny language"}

$seventeen=&$foo
$seventeen

$return ={return 17; Write-Host "Powershell is some funny language"}

$test =&$return

$test

$qa={
    $question=$args[0]
    $answer=$args[1]
    Write-Host "Here is your question: $question the answer is $answer"
}

$qa={
    param($question,$answer)
    Write-Host "Here is your question: $question the answer is $answer"
}

&$qa -answer "powershell!!" -question "what is cool?" 

$qa={
    param($question,$answer="the question has no answer")
    Write-Host "Here is your question: $question the answer is $answer"
}

&$qa -question "what is cool?" 

$math ={
    param([int] $x, [int] $y)
    return $x+$y
}

&$math 1 9

#create custom piping
Set-Location  "C:\francis_source\DevOps"
$onlycoolfiles ={
    process {
        if($_.Name -like "*.ps1"){
            return $_.Name
        }
    }
}

Get-ChildItem | &$onlycoolfiles

$onlycoolfiles ={
    begin {$text="here are some cool file `r`n"}
    process {
        if($_.Name -like "*.ps1"){
            return $_.Name
        }
    }
    end {return $text}
}

Get-ChildItem | &$onlycoolfiles

$onlycoolfiles ={
    param($headertext)
    begin {$text="$headertext `r`n"}
    process {
        if($_.Name -like "*.ps1"){
            return $_.Name
        }
    }
    end {return $text}
}

Get-ChildItem | &$onlycoolfiles -headertext "we only deal with cool file"

