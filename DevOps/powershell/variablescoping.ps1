﻿$var = 41

Get-Variable var -ValueOnly

&{
    Set-Variable var 33 -Scope 1
    Write-Host "inside the script block: $var"
}

 Write-Host "outside the script block: $var"