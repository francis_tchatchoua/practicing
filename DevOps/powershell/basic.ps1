﻿#Getting help
Get-Command -Verb "get"
Get-Command -Noun "service"

Get-Help Get-Command -Examples
Get-Help Get-Command -Detailed
Get-Help Get-Command -Full
Get-Command -?

#change directory
Set-Location C:\francis_source

#playing with piping
Get-ChildItem | Where-Object {$_.Length -gt 100kb} | sort-object length | Format-Table -Property Name, Length -AutoSize
Get-ChildItem | sort-object length | Format-Table -Property Name, Length -AutoSize
Get-ChildItem | sort-object length | Select-Object Name,Length