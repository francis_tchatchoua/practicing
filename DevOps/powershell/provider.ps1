﻿Clear-Host
Get-PSProvider

Clear-Host
Get-PSDrive

Clear-Host
Set-Location env:
Get-ChildItem

Get-ChildItem | Format-Table -Property Name, Value -AutoSize

Clear-Host
Set-Location alias:
Get-ChildItem

#create custom providers
Clear-Host
Get-PSSnapin -Registered
