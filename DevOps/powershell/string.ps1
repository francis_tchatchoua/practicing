﻿Set-Location C:\francis_source

$items = (Get-ChildItem).Length
$loc = Get-Location

"there are $items items in this directory $loc"
"there are `$items items in this directory `$loc"

"there are {0} items in this directory {1} percentage {2:0.00}" -f $items,$loc,$(33*0.01)

"tchatchoua" -like "tchat*"
"tchatchoua" -like "?chatchoua"

"888-226-772" -match "[0-9]{3}-[0-9]{3}-[0-9]{3}"
"888-226-7u2" -match "[0-9]{3}-[0-9]{3}-[0-9]{3}"
