import { Component } from '@angular/core';
import { ComponentFixture, TestBed,tick,fakeAsync,async } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

class UserService {
  isLoggedIn:boolean;
  user:any;
  getQuote():Promise<any>{
    return null;
  }
}

let userServiceStub :UserService ={
  isLoggedIn:false,
  user:{
    name:'pat'
  },
  getQuote():Promise<any>{
    return null;
  }
}

@Component({
  selector: 'app-banner',
  template: `<h1>{{title}}</h1>
            <p>{{message}}</p>
            <p class="quote">{{quote}}</p>`
})
export class BannerComponent {
  message = '-- not initialized yet --';
  title = 'Test Tour of Heroes';
  quote = '...';
  constructor(private service:UserService){}

   ngOnInxit(): void {
    this.message = this.service.isLoggedIn ?
      'Welcome, ' + this.service.user.name :
      'Please log in.';
    
    this.service.getQuote().then(quote => this.quote = quote);
  }
}

describe('BannerComponent (inline template)', () => {

  let comp:    BannerComponent;
  let fixture: ComponentFixture<BannerComponent>;
  let de:      DebugElement;
  let el:      HTMLElement;

  let de2:      DebugElement;
  let el2:      HTMLElement;
  let service:UserService;
  let spy:any;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      declarations: [ BannerComponent ],
      providers:    [ {provide: UserService, useValue: userServiceStub } ]
    })
    .compileComponents();
    
  });

  beforeEach(() => {

    fixture = TestBed.createComponent(BannerComponent);
    comp = fixture.componentInstance;
    
    de = fixture.debugElement.query(By.css('h1'));
    el = de.nativeElement;

     service = fixture.debugElement.injector.get(UserService);

     spy = spyOn(service, 'getQuote')
        .and.returnValue(Promise.resolve(6727727));

     de2 = fixture.debugElement.query(By.css('.quote'));
     el2= de2.nativeElement;
  });

  xit('should display original title', () => {
    fixture.detectChanges();
    expect(el.textContent).toContain(comp.title);
  });

  xit('should display a different test title', () => {
    comp.title = 'Test Title';
    fixture.detectChanges();
    expect(el.textContent).toContain('Test Title');
  });

  xit('no title in the DOM until manually call `detectChanges`', () => {
    expect(el.textContent).toEqual('');
  });

  xit('should return a valid user service', () => {
    let  userService = fixture.debugElement.injector.get(UserService);
    expect(userService).toBeTruthy();
  });

  xit('should not show quote before OnInit', () => {
    expect(el2.textContent).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getQuote not yet called');
  });

  xit('should still not show quote after component initialized', () => {
    fixture.detectChanges();
    // getQuote service is async => still has not returned with quote
    expect(el2.textContent).toBe('...', 'no quote yet');
    expect(spy.calls.any()).toBe(true, 'getQuote called');
  });

  xit('should show quote after getQuote promise (async)', async(() => {
    fixture.detectChanges();
 
    fixture.whenStable().then(() => { // wait for async getQuote
      fixture.detectChanges();        // update view with quote
      expect(el2.textContent).toBe('6727727');
    });

  }));
  xit('should show quote after getQuote promise (fakeAsync)', fakeAsync(() => {
    fixture.detectChanges();
    tick();                  // wait for async getQuote
    fixture.detectChanges(); // update view with quote
    expect(el2.textContent).toBe('6727727');
  }));
});