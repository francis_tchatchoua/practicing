using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using HttpMultipartParser;
using Interoute.Global.Extensions;
using Interoute.Global.Logging.Exceptions;
using Interoute.Tickets.API.DTOs;
using Interoute.Tickets.Domain.Entities;

namespace Interoute.Hub.UI.BootstrapSupport
{
    public class IncidentMultipartTypeFormater : MediaTypeFormatter
    {
        public IncidentMultipartTypeFormater()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/octet-stream"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));
        }

        public override bool CanReadType(Type type)
        {
            var canRead = type == typeof(IncidentDetailsDTO);
            return canRead;
        }

        public override bool CanWriteType(Type type)
        {
            var canWrite = type == typeof(IncidentDetailsDTO);
            return canWrite;
        }

        public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content,
                                                               IFormatterLogger formatterLogger)
        {
            // parse:
            var parser = new MultipartFormDataParser(readStream);
            var incident = SerializationExtentender.DeSerializeJson<IncidentDetailsDTO>(
                    parser.Parameters.FirstOrDefault().Value.Data);
            if (parser.Files != null && parser.Files.Count>0 && !string.IsNullOrWhiteSpace(parser.Files[0].FileName))
            {
                var fileContent = parser.Files[0];
                var fileName = string.IsNullOrWhiteSpace(incident.AttachmentName)
                                   ? fileContent.FileName
                                   : incident.AttachmentName;

                incident.TemporaryFile = new byte[fileContent.Data.Length];

                fileContent.Data.Read(incident.TemporaryFile, 0, (int) fileContent.Data.Length);

                var file = new TicketAttachment()
                    {
                        FileName = Path.GetFileNameWithoutExtension(fileName),
                        Description = fileContent.ContentType,
                        FileExtension = Path.GetExtension(fileName) != null ? Path.GetExtension(fileName).Trim('.') : null,
                        Id = Guid.NewGuid().ToString(),
                        FileSize = Convert.ToDecimal(fileContent.Data.Length)
                    };

                incident.Attachment = file;
            }
            return Task.FromResult((object)incident);
        }
    }
}
